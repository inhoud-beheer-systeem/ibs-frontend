# Stage 0: Build Angular
FROM node:erbium-alpine AS build
ARG buildenv
ARG NPM_TOKEN

WORKDIR /usr/build
COPY package.json package-lock.json* ./
RUN npm ci
COPY . .
RUN npm run build -- --configuration=$buildenv

# Stage 1: Serve Angular
FROM nginx:stable

COPY --from=build /usr/build/dist/* /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.template
COPY docker-entrypoint.sh /
RUN chmod +x docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
