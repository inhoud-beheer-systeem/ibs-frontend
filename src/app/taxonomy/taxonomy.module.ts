import { MomentModule } from 'ngx-moment';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { DragulaModule } from 'ng2-dragula';

import { UiModule } from '../../lib/ui/ui.module';
import { TaxonomyRoutingModule } from './taxonomy-routing.module';
import { Pages } from './pages';
import { Forms } from './forms';

@NgModule({
    declarations: [
        Pages,
        Forms
    ],
    imports: [
        DragulaModule,
        CommonModule,
        TaxonomyRoutingModule,
        RouterModule,
        UiModule,
        ReactiveFormsModule,
        MomentModule,
        MatDialogModule
    ]
})
export class TaxonomyModule { }
