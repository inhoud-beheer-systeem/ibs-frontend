import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { pathOr } from 'ramda';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { propOr } from 'ramda';
import { DragulaService } from 'ng2-dragula';
import slugify from 'slugify';

@Component({
    selector: 'app-taxonomy-form',
    templateUrl: 'taxonomy.form.html',
})
export class TaxonomyFormComponent implements OnInit, OnChanges, OnDestroy {
    @Input() taxonomy: any[];

    @Output() formChange: EventEmitter<any> = new EventEmitter<any>();

    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
    ) {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required],
            slug: ['', Validators.required],
            items: this.formBuilder.array([])
        });

        this.form.get('name').valueChanges
            .pipe(
                takeUntil(this.componentDestroyed$)
            )
            .subscribe((name) => this.form.patchValue({ slug: slugify(name).toLowerCase() }));
    }

    public ngOnInit(): void {
        this.form.valueChanges
            .pipe(
                takeUntil(this.componentDestroyed$)
            )
            .subscribe((values) => this.formChange.emit(values));
    }

    public ngOnChanges(changes: any) {
        if (!changes.taxonomy) {
            return;
        }

        const taxonomy = changes.taxonomy.currentValue;

        if (!taxonomy) {
            return;
        }

        this.form.patchValue(taxonomy);

        const fieldsGroup = this.form.get('items') as FormArray;
        this.buildItemsArray((propOr([], 'items')(taxonomy) as any[])).map((formGroup) => {
            fieldsGroup.push(formGroup);
        });
    }

    private buildItemsArray(items) {
        return items.map((item) => {
            const fieldForm = this.formBuilder.group({
                value: [item.value],
                label: [item.label],
            });

            return fieldForm;
        });
    }

    public removeField(e: Event, i: number) {
        e.preventDefault();
        const fieldsGroup = this.form.get('items') as FormArray;
        fieldsGroup.removeAt(i);
    }

    public pushFieldToArray(e: Event): void {
        e.preventDefault();

        const fieldForm = this.formBuilder.group({
            value: [''],
            label: [''],
        });

        (this.form.get('items') as FormArray).push(fieldForm);
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
