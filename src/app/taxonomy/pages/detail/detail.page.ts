import { ToastrService } from 'ngx-toastr';
import { Subject, Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TaxonomyService } from '../../../../lib/store';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public taxonomy: any;
    public updatedTaxonomy: any;

    constructor(
        private taxonomyService: TaxonomyService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService
    ) { }

    public ngOnInit(): void {
        this.taxonomyService.fetchOne(this.activatedRoute.snapshot.params.id)
            .pipe(
                first()
            ).subscribe((taxonomy) => {
                this.updatedTaxonomy = taxonomy;
                this.taxonomy = taxonomy;
            });
    }

    public handleFormChange(values) {
        this.updatedTaxonomy = values;
    }

    public submit(e: Event) {
        e.preventDefault();
        this.taxonomyService.update(this.activatedRoute.snapshot.params.id, this.updatedTaxonomy)
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.success('Taxonomy updated', 'Success');
            });
    }

    public delete(e: Event) {
        e.preventDefault();
        this.taxonomyService.delete(this.activatedRoute.snapshot.params.id)
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.warning('Taxonomy deleted', 'Success');
                this.router.navigate(['/taxonomy']);
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
