import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { TaxonomyService } from '../../../../lib/store';

@Component({
    templateUrl: './create.page.html'
})
export class CreatePageComponent implements OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public taxonomy: any[];
    public openFields = {};

    constructor(
        private taxonomyService: TaxonomyService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    public handleFormUpdate(values) {
        this.taxonomy = values;
    }

    public submit(e: Event) {
        e.preventDefault();
        this.taxonomyService.create(this.taxonomy)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('Taxonomy created', 'Success');
                this.router.navigate(['/taxonomy', result.uuid]);
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
