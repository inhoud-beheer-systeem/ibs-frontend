import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';

import { TaxonomyQuery, TaxonomyService } from '../../../../lib/store';

@Component({
    templateUrl: './list.page.html'
})
export class ListPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();
    public taxonomy$;

    constructor(
        private taxonomyService: TaxonomyService,
        private taxonomyQuery: TaxonomyQuery
    ) { }

    public ngOnInit(): void {
        this.taxonomy$ = this.taxonomyQuery.selectAll();
        this.taxonomyService.fetch()
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe();
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
