import { TokenInterceptor } from './token.interceptor';

export { TokenInterceptor } from './token.interceptor';

export const Interceptors = [
    TokenInterceptor
];
