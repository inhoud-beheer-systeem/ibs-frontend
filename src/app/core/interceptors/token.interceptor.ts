import { pathOr } from 'ramda';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import {
    HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private authService: AuthService,
        private router: Router,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.authService.getToken()}`,
                'X-Tenant': pathOr('unset', ['selectedTenant', 'uuid'])(this.authService) as string
            }
        });

        return next.handle(request)
            .pipe(
                tap(
                    () => { },
                    (err: any) => {
                        if (err instanceof HttpErrorResponse) {
                            if (err.status !== 401) {
                                return;
                            }

                            localStorage.removeItem('token');
                            this.router.navigate(['auth', 'login']);
                        }
                    }
                )
            );
    }
}
