import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService, UserService } from '../../services';

@Component({
    selector: 'app-wrapper',
    templateUrl: './wrapper.component.html'
})
export class WrapperComponent implements OnInit {
    public user: any;

    constructor(
        public authService: AuthService,
        public userService: UserService,
        private router: Router,
    ) { }

    public ngOnInit(): void {
        this.userService.fetchUser();

        this.user = this.userService.user;
    }

    public handleTenantChange(tenant: any) {
        this.authService.selectTenant(tenant.uuid);
    }

    public handleLogout() {
        this.authService.logout();
        this.router.navigate(['auth', 'login']);
    }
}
