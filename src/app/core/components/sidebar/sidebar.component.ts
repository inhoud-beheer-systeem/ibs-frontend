import { Observable, Subject } from 'rxjs';
import { first, map, takeUntil, tap } from 'rxjs/operators';

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { SessionQuery, StatusQuery, StatusService } from '../../../../lib/store';
import { ContentTypeQuery, ContentTypeService, PageTypeQuery, PageTypeService } from '../../store';
import { coreLinks, tenantLinks } from './sidebar.const';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit, OnDestroy {
    @Input() tenants;

    @Output() logout: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() tenantSelected: EventEmitter<any> = new EventEmitter<any>();

    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();
    public contentTypes$;
    public pageTypes$;
    public status$: Observable<any>;
    public tenantSelectorOpen = false;
    public coreLinks: any = [];
    public tenantLinks: any = [];
    public tenant$: Observable<any>;
    public user$: Observable<any>;
    public permissions$: Observable<any[]>;
    public open = false;

    constructor(
        private contentTypeService: ContentTypeService,
        private contentTypeQuery: ContentTypeQuery,
        private pageTypeService: PageTypeService,
        private pageTypeQuery: PageTypeQuery,
        public sessionQuery: SessionQuery,
        public statusService: StatusService,
        public statusQuery: StatusQuery,
    ) { }

    public ngOnInit(): void {
        this.contentTypes$ = this.contentTypeQuery.selectAll()
            .pipe(
                map((contentTypes) => {
                    return contentTypes.map((contentType) => ({
                        ...contentType,
                        link: `/content/${contentType.slug}`
                    }));
                })
            );

        this.pageTypes$ = this.pageTypeQuery.selectAll()
            .pipe(
                map((pageTypes) => {
                    return pageTypes.map((pageType) => ({
                        ...pageType,
                        link: `/pages/${pageType.slug}`
                    }));
                })
            );

        this.statusService.fetch().subscribe();

        this.user$ = this.sessionQuery.user$;
        this.permissions$ = this.sessionQuery.permissions$;
        this.status$ = this.statusQuery.status$;
        this.tenant$ = this.sessionQuery.tenant$
            .pipe(
                tap(() => this.fetchGlobals())
            );

        this.permissions$.subscribe((permissions) => {
            this.tenantLinks = tenantLinks(permissions).filter((x) => x.show);
            this.coreLinks = coreLinks(permissions).filter((x) => x.show);
        });
    }

    public toggleOpen(e: Event) {
        this.open = !this.open;
    }

    public chooseTenant(e: Event, tenant: any) {
        e.preventDefault();
        this.tenantSelectorOpen = false;
        this.tenantSelected.emit(tenant);
    }

    public toggleTenantSelector(e: Event) {
        e.preventDefault();
        this.tenantSelectorOpen = !this.tenantSelectorOpen;
    }

    public handleLogout(e: Event) {
        e.preventDefault();
        this.logout.emit(true);
    }

    private fetchGlobals() {
        this.contentTypeService.fetch()
            .pipe(
                first()
            ).subscribe();

        this.pageTypeService.fetch()
            .pipe(
                first()
            ).subscribe();
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
