export const coreLinks = (permissions) => [
    {
        icon: 'home',
        name: 'Dashboard',
        link: '/dashboard',
        show: permissions.includes('dashboard/read')
    },
    {
        icon: 'image',
        name: 'Resources',
        link: '/resources',
        show: permissions.includes('storage/list')
    },
    {
        icon: 'list-ul',
        name: 'Views',
        link: '/views',
        show: permissions.includes('views/read')
    },
    {
        icon: 'list-ui-alt',
        name: 'Taxonomy',
        link: '/taxonomy',
        show: permissions.includes('taxonomy/read')
    }
];

export const tenantLinks = (permissions) => [
    {
        icon: 'file',
        name: 'Page Types',
        link: '/page-types',
        show: permissions.includes('page-types/read')
    },
    {
        icon: 'document-layout-left',
        name: 'Content Types',
        link: '/content-types',
        show: permissions.includes('content-types/read')
    },
    {
        icon: 'users-alt',
        name: 'Users',
        link: '/tenant/users',
        show: permissions.includes('users/read')
    },
    {
        icon: 'label-alt',
        name: 'Roles',
        link: '/tenant/roles',
        show: permissions.includes('roles/read')
    },
    {
        icon: 'comments',
        name: 'Languages',
        link: '/tenant/languages',
        show: permissions.includes('languages/read')
    },
    {
        icon: 'code-branch',
        name: 'Workflows',
        link: '/tenant/workflows',
        show: true
    },
];
