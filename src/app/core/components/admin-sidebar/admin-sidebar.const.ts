export const adminLinks = (permissions) => [
    {
        icon: 'user-group',
        name: 'Users',
        link: '/admin/users',
        show: true,
    },
    {
        icon: 'stack',
        name: 'Tenants',
        link: '/admin/tenants',
        show: true,
    }
];
