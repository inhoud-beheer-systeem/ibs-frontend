import { CoreComponent } from './core/core.component';
import { MenuComponent } from './menu/menu.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ToastComponent } from './toast/toast.component';
import { WrapperComponent } from './wrapper/wrapper.component';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
import { AdminWrapperComponent } from './admin-wrapper/admin-wrapper.component';

export { CoreComponent } from './core/core.component';
export { MenuComponent } from './menu/menu.component';
export { SidebarComponent } from './sidebar/sidebar.component';
export { ToastComponent } from './toast/toast.component';
export { WrapperComponent } from './wrapper/wrapper.component';
export { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
export { AdminWrapperComponent } from './admin-wrapper/admin-wrapper.component';

export const Components = [
    CoreComponent,
    MenuComponent,
    SidebarComponent,
    ToastComponent,
    WrapperComponent,
    AdminSidebarComponent,
    AdminWrapperComponent
];

export const EntryComponents = [
    ToastComponent
];
