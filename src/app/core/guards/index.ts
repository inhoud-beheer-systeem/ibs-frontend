import { AuthGuard } from './auth.guard';

export { AuthGuard } from './auth.guard';

export const Guards = [
    AuthGuard
];
