import { ToastrService } from 'ngx-toastr';
import { prop, path, propOr } from 'ramda';
import { throwError } from 'rxjs';
import { catchError, tap, first } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { SessionService } from '../../../lib/store';

@Injectable()
export class AuthService {
    public tenants;
    public selectedTenant;

    constructor(
        private jwtHelper: JwtHelperService,
        private http: HttpClient,
        private toastr: ToastrService,
        private sessionService: SessionService,
    ) {
        const tokenResult = this.jwtHelper.decodeToken(this.getToken());
        this.tenants = path(['tenants'])(tokenResult);
        this.orchestrateTenantSelection();
    }

    public isAuthenticated(): boolean {
        const token = localStorage.getItem('token');
        return !this.jwtHelper.isTokenExpired(token);
    }

    public getToken(): any {
        return localStorage.getItem('token');
    }

    public selectTenant(tenantUuid: string) {
        localStorage.setItem('selectedTenant', tenantUuid);
        this.orchestrateTenantSelection();
    }

    public login(values) {
        return this.http.post<any>('/api/v1/auth/login', values)
            .pipe(
                catchError((err) => {
                    this.toastr.error('Please check all your details', 'Something went wrong');

                    return throwError(err);
                }),
                tap((result) => {
                    const tokenResult = this.jwtHelper.decodeToken(result.token);
                    this.tenants = prop('tenants')(tokenResult);
                    localStorage.setItem('token', result.token);
                    this.orchestrateTenantSelection();
                })
            );
    }

    public requestPasswordReset(values: any) {
        return this.http.post<any>('/api/v1/auth/password-reset', values)
            .pipe(
                catchError((err) => {
                    this.toastr.error('Could not reset your password', 'Something went wrong');

                    return throwError(err);
                })
            );
    }

    private orchestrateTenantSelection() {
        const tenantUuid = localStorage.getItem('selectedTenant');

        if (!this.tenants) {
            return;
        }

        if (!tenantUuid) {
            this.selectedTenant = this.tenants[0];
            this.sessionService.updateTenant(this.selectedTenant);
            return;
        }

        this.selectedTenant = this.tenants.find((t) => t.uuid === tenantUuid);
        this.sessionService.updateTenant(this.selectedTenant);
    }

    public logout() {
        localStorage.removeItem('token');
        this.toastr.success('Logged out successfully');
    }
}
