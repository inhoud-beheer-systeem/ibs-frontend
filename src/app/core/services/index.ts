import { AuthService } from './auth.service';
import { UserService } from './user.service';

export { AuthService } from './auth.service';
export { UserService } from './user.service';

export const Services = [
    AuthService,
    UserService,
];
