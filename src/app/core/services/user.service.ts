import { tap, first } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SessionService } from '../../../lib/store';

@Injectable()
export class UserService {
    public user: any;
    public permissions: any[];

    constructor(
        private http: HttpClient,
        private sessionService: SessionService,
    ) {
        this.fetchUser();
    }

    public fetchUser() {
        this.http.get<any>('/api/v1/auth/user')
            .pipe(
                tap((result) => {
                    this.user = result.user;
                    this.permissions = result.permissions;
                    this.sessionService.updateUser(this.user);
                    this.sessionService.updatePermissions(this.permissions);
                }),
                first()
            ).subscribe();
    }
}
