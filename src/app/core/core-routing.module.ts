import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import * as Components from './components';
import { AuthGuard } from './guards';
import * as Pages from './pages';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: 'onboarding',
        loadChildren: () => import('../onboarding/onboarding.module').then(m => m.OnboardingModule)
    },
    {
        path: '',
        component: Components.WrapperComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'dashboard',
                component: Pages.DashboardPageComponent,
            },
            {
                path: 'content-types',
                loadChildren: () => import('../content-types/content-types.module').then(m => m.ContentTypesModule),
            },
            {
                path: 'page-types',
                loadChildren: () => import('../page-types/page-types.module').then(m => m.PageTypesModule),
            },
            {
                path: 'content',
                loadChildren: () => import('../content/content.module').then(m => m.ContentModule),
            },
            {
                path: 'taxonomy',
                loadChildren: () => import('../taxonomy/taxonomy.module').then(m => m.TaxonomyModule),
            },
            {
                path: 'pages',
                loadChildren: () => import('../pages/pages.module').then(m => m.PagesModule),
            },
            {
                path: 'resources',
                loadChildren: () => import('../resources/resources.module').then(m => m.ResourcesModule),
            },
            {
                path: 'views',
                loadChildren: () => import('../views/views.module').then(m => m.ViewsModule),
            },
            {
                path: 'tenant/users',
                loadChildren: () => import('../users/users.module').then(m => m.UsersModule),
            },
            {
                path: 'tenant/roles',
                loadChildren: () => import('../roles/roles.module').then(m => m.RolesModule),
            },
            {
                path: 'tenant/languages',
                loadChildren: () => import('../languages/languages.module').then(m => m.LanguagesModule),
            },
            {
                path: 'tenant/workflows',
                loadChildren: () => import('../workflows/workflows.module').then(m => m.WorkflowsModule),
            }
        ]
    },
    {
        path: 'admin',
        component: Components.AdminWrapperComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'tenants',
                loadChildren: () => import('../admin/tenants/tenants.module').then(m => m.TenantsModule),
            },
            {
                path: 'users',
                loadChildren: () => import('../admin/users/admin-users.module').then(m => m.AdminUsersModule),
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
