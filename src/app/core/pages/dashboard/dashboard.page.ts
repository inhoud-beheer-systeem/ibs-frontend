import { curveBasis } from 'd3-shape';
import { Observable } from 'rxjs';

import { Component, OnInit } from '@angular/core';

import { DashboardQuery, DashboardService } from '../../store';

@Component({
    templateUrl: './dashboard.page.html'
})
export class DashboardPageComponent implements OnInit {
    public data$: Observable<any>;
    public colorScheme = {
        domain: ['#78e08f']
    };
    public curve: any = curveBasis;

    constructor(
        private dashboardService: DashboardService,
        private dashboardQuery: DashboardQuery
    ) { }

    ngOnInit(): void {
        this.dashboardService.fetchDashboards()
            .subscribe();

        this.data$ = this.dashboardQuery.results$;
    }
}
