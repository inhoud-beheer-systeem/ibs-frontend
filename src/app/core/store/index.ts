import { ContentFieldQuery } from './content-field/content-field.query';
import { ContentFieldService } from './content-field/content-field.service';
import { ContentFieldStore } from './content-field/content-field.store';
import { ContentTypeService } from './content-types/content-type.service';
import { ContentTypeStore } from './content-types/content-type.store';
import { ContentTypeQuery } from './content-types/content-types.query';
import { DashboardQuery } from './dashboard/dashboard.query';
import { DashboardService } from './dashboard/dashboard.service';
import { DashboardStore } from './dashboard/dashboard.store';
import { PageTypeService } from './page-types/page-type.service';
import { PageTypeStore } from './page-types/page-type.store';
import { PageTypeQuery } from './page-types/page-types.query';

export { ContentFieldQuery } from './content-field/content-field.query';
export { ContentFieldService } from './content-field/content-field.service';
export { ContentFieldStore } from './content-field/content-field.store';
export { ContentTypeService } from './content-types/content-type.service';
export { ContentTypeStore } from './content-types/content-type.store';
export { ContentTypeQuery } from './content-types/content-types.query';
export { PageTypeService } from './page-types/page-type.service';
export { PageTypeStore } from './page-types/page-type.store';
export { PageTypeQuery } from './page-types/page-types.query';
export { DashboardService } from './dashboard/dashboard.service';
export { DashboardStore } from './dashboard/dashboard.store';
export { DashboardQuery } from './dashboard/dashboard.query';

export const StoreServices = [
    ContentFieldService,
    ContentTypeService,
    PageTypeService,
    DashboardService
];

export const Stores = [
    ContentFieldStore,
    ContentTypeStore,
    PageTypeStore,
    DashboardStore
];

export const Queries = [
    ContentFieldQuery,
    ContentTypeQuery,
    PageTypeQuery,
    DashboardQuery
];

