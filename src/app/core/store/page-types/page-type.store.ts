import { EntityState, EntityStore, ID, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';

export interface PageType {
    uuid: string;
    name: string;
    link: string;
    slug: string;
    showOnOverview: boolean;
    multiLanguage: boolean;
    description: string;
    fields: any[];
}

export interface PageTypeState extends EntityState<PageType> { }

@Injectable()
@StoreConfig({ name: 'pageTypes', idKey: 'slug' })
export class PageTypeStore extends EntityStore<PageTypeState, PageType> {
  constructor() {
    super();
  }
}
