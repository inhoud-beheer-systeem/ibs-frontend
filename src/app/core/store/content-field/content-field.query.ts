import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';

import { ContentField, ContentFieldStore } from './content-field.store';

@Injectable()
export class ContentFieldQuery extends Query<ContentField> {
    public results$ = this.select((state) => (state as any).result);
    public loading$ = this.select((state) => (state as any).loading);

    constructor(protected store: ContentFieldStore) {
        super(store);
    }
}
