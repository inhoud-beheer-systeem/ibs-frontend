import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ContentFieldStore } from './content-field.store';

@Injectable()
export class ContentFieldService {

    constructor(
        private contentFieldStore: ContentFieldStore,
        private http: HttpClient
    ) { }

    fetchContentFields() {
        this.contentFieldStore.setLoading(true);
        return this.http.get('/api/v1/field-types')
            .pipe(
                tap(fields => {
                    this.contentFieldStore.update({
                        result: fields
                    });
                    this.contentFieldStore.setLoading(false);
                })
            );
  }
}
