import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

export interface ContentField {
   name: string;
   component: string;
}

export function createInitialState(): ContentField[] {
  return [];
}

@Injectable()
@StoreConfig({ name: 'contentFields' })
export class ContentFieldStore extends Store<any> {
  constructor() {
    super(createInitialState());
  }
}
