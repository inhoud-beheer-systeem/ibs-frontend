import { ToastrService } from 'ngx-toastr';
import { prop } from 'ramda';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

import { AuthService } from '../../../core/services';

@Component({
    templateUrl: './login.page.html'
})
export class LoginPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public resources$;
    public folder: string[] = [];
    public form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private toastr: ToastrService,
        private jwtHelper: JwtHelperService,
        private router: Router,
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    public handleSubmit(e: Event) {
        e.preventDefault();

        this.authService.login(this.form.value)
            .pipe(
                first()
            )
            .subscribe(({ token }) => {
                const user = prop('user')(this.jwtHelper.decodeToken(token)) as any;
                this.toastr.success(`Welcome back ${user.firstName}`, 'Success');
                this.router.navigate(['dashboard']);
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
