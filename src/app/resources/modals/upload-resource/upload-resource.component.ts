import { Observable } from 'rxjs';

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { ResourceService } from '../../../../lib/store';

@Component({
    templateUrl: 'upload-resource.component.html',
})
export class UploadResourceModalComponent implements OnInit {
    public contentFields$: any;
    public loading$: Observable<boolean>;

    constructor(
        private resourceService: ResourceService,
        public dialogRef: MatDialogRef<UploadResourceModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    public ngOnInit(): void {
    }

    public handleFiles(event: any): void {
        [...event.target.files].forEach((file: File) => {
            console.log(file);
            this.resourceService.upload(this.data.dir, file)
                .subscribe();
        });

        this.dialogRef.close();
    }

    close(e: Event): void {
        e.preventDefault();
        this.dialogRef.close();
    }
}
