import {
    CreateDirectoryModalComponent
} from './create-directory/create-directory.component';
import {
    UploadResourceModalComponent
} from './upload-resource/upload-resource.component';

export {
    CreateDirectoryModalComponent
} from './create-directory/create-directory.component';
export {
    UploadResourceModalComponent
} from './upload-resource/upload-resource.component';

export const Modals = [
    CreateDirectoryModalComponent,
    UploadResourceModalComponent
];
