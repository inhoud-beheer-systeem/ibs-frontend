import { Observable } from 'rxjs';

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    templateUrl: 'create-directory.component.html',
})
export class CreateDirectoryModalComponent implements OnInit {
    public form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<CreateDirectoryModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            name: ['', Validators.required]
        });
    }

    submit(): void {
        this.form.markAllAsTouched();
        if (!this.form.valid) {
            return;
        }

        this.dialogRef.close(this.form.value);
    }

    close(): void {
        this.dialogRef.close();
    }
}
