import { ToastrService } from 'ngx-toastr';
import { Observable, Subject } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { PermissionQuery, PermissionService, RoleService } from '../../../../lib/store';

@Component({
    templateUrl: './create.page.html'
})
export class CreatePageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public permissions$: Observable<any>;

    constructor(
        private roleService: RoleService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
        private permissionService: PermissionService,
        private permissionQuery: PermissionQuery,
    ) { }

    public ngOnInit(): void {
        this.permissions$ = this.permissionQuery.results$;
        this.permissionService.fetch()
            .pipe(
                first()
            ).subscribe((permissions) => {
                const permissionFormGroup = permissions
                    .reduce((acc, group) => {
                        acc.push(...group.permissions);

                        return acc;
                    }, [])
                    .reduce((acc, permission) => {
                        return {
                            ...acc,
                            [permission.value]: [false]
                        };
                    }, {});

                this.form = this.formBuilder.group({
                    name: ['', Validators.required],
                    permissions: this.formBuilder.group(permissionFormGroup),
                });
            });
    }

    public submit(e: Event) {
        e.preventDefault();

        this.roleService.create(this.form.value)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('Role created', 'Success');
                this.router.navigate(['../', result.uuid], {
                    relativeTo: this.activatedRoute
                });
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
