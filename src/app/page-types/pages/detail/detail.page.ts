import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { ContentFieldService, PageTypeService } from '../../../core/store';
import { WorkflowService } from '../../../../lib/store';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public pageType: any;
    public contentFields: any[];
    public workflows = [];
    public formValues: any;

    constructor(
        private pageTypeService: PageTypeService,
        private contentFieldService: ContentFieldService,
        private workflowService: WorkflowService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService
    ) { }

    public ngOnInit(): void {
        this.workflowService.fetch()
            .pipe(
                first()
            ).subscribe((workflows) => {
                this.workflows = workflows._embedded.map((workflow) => ({
                    value: workflow.uuid,
                    label: workflow.name
                }));
            });

        this.contentFieldService.fetchContentFields()
            .pipe(
                first()
            ).subscribe((contentFields) => {
                this.contentFields = contentFields as any;
                this.pageTypeService.fetchOne(this.activatedRoute.snapshot.params.id)
                    .pipe(
                        first()
                    ).subscribe((pageType) => {
                        this.pageType = pageType;
                        this.formValues = pageType;
                    });
            });
    }

    public handleFormChange(values) {
        this.formValues = values;
    }

    public submit(e: Event) {
        e.preventDefault();
        this.pageTypeService.update(this.activatedRoute.snapshot.params.id, this.formValues)
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.success('Page type updated', 'Success');
            });
    }

    public delete(e: Event) {
        e.preventDefault();
        this.pageTypeService.delete(this.activatedRoute.snapshot.params.id)
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.warning('Page type deleted', 'Success');
                this.router.navigate(['/content-types']);
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
