import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import slugify from 'slugify';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ContentFieldQuery, PageTypeService } from '../../../core/store';
import { WorkflowService } from '../../../../lib/store';

@Component({
    templateUrl: './create.page.html'
})
export class CreatePageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public contentFields: any[];
    public workflows = [];
    public formValues: any;

    constructor(
        private pageTypesService: PageTypeService,
        private contentFieldQuery: ContentFieldQuery,
        private workflowService: WorkflowService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    public ngOnInit(): void {
        this.workflowService.fetch()
            .pipe(
                first()
            ).subscribe((workflows) => {
                this.workflows = workflows._embedded.map((workflow) => ({
                    value: workflow.uuid,
                    label: workflow.name
                }));
            });

        this.contentFieldQuery.results$
            .pipe(
                takeUntil(this.componentDestroyed$)
            )
            .subscribe((contentFields) => this.contentFields = contentFields);
    }

    public handleFormChange(values) {
        this.formValues = values;
    }

    public submit(e: Event) {
        e.preventDefault();
        this.pageTypesService.create(this.formValues)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('Page type created', 'Success');
                this.router.navigate(['/page-types', result.uuid]);
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
