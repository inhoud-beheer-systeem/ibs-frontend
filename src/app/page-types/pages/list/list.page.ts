import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';

import { PageTypeQuery, PageTypeService } from '../../../core/store';

@Component({
    templateUrl: './list.page.html'
})
export class ListPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();
    public pageTypes$;

    constructor(
        private pageTypeService: PageTypeService,
        private pageTypeQuery: PageTypeQuery
    ) { }

    public ngOnInit(): void {
        this.pageTypes$ = this.pageTypeQuery.selectAll();
        this.pageTypeService.fetch()
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe();
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
