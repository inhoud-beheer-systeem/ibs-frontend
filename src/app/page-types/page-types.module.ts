import { MomentModule } from 'ngx-moment';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { DragulaModule } from 'ng2-dragula';

import { UiModule } from '../../lib/ui/ui.module';
import { Components } from './components';
import { Modals } from './modals';
import { PageTypesRoutingModule } from './page-types-routing.module';
import { Pages } from './pages';

@NgModule({
    declarations: [
        Components,
        Modals,
        Pages
    ],
    imports: [
        DragulaModule,
        CommonModule,
        PageTypesRoutingModule,
        RouterModule,
        UiModule,
        ReactiveFormsModule,
        MomentModule,
        MatDialogModule
    ],
    entryComponents: [
        Modals,
    ]
})
export class PageTypesModule { }
