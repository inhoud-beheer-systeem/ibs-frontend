import { MomentModule } from 'ngx-moment';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { DragulaModule } from 'ng2-dragula';

import { UiModule } from '../../lib/ui/ui.module';
import { Modals } from './modals';
import { WorkflowsRoutingModule } from './workflows-routing.module';
import { Pages } from './pages';

@NgModule({
    declarations: [
        Modals,
        Pages
    ],
    imports: [
        DragulaModule,
        CommonModule,
        WorkflowsRoutingModule,
        RouterModule,
        UiModule,
        ReactiveFormsModule,
        MomentModule,
        MatDialogModule
    ],
    entryComponents: [
        Modals,
    ]
})
export class WorkflowsModule { }
