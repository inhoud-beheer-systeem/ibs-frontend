import { Observable } from 'rxjs';

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';

@Component({
    templateUrl: 'action-creator.component.html',
})
export class ActionCreatorModalComponent implements OnInit {
    public form;

    constructor(
        public dialogRef: MatDialogRef<ActionCreatorModalComponent>,
        public formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            action: [''],
            newState: ['']
        });
    }

    public close(e: Event): void {
        e.preventDefault();
        this.dialogRef.close();
    }

    public getExistingStates() {
        return this.data.states.map((state) => ({
            value: state.slug,
            label: state.name,
        }));
    }

    public save(): void {
        this.dialogRef.close(this.form.value);
    }
}
