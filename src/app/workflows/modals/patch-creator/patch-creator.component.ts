import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';

@Component({
    templateUrl: 'patch-creator.component.html',
})
export class PatchCreatorModalComponent implements OnInit {
    public form;

    constructor(
        public dialogRef: MatDialogRef<PatchCreatorModalComponent>,
        public formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            path: [''],
            value: ['']
        });
    }

    public close(e: Event): void {
        e.preventDefault();
        this.dialogRef.close();
    }

    public save(): void {
        this.dialogRef.close(this.form.value);
    }
}
