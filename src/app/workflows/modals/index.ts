import { ActionCreatorModalComponent } from './action-creator/action-creator.component';
import { PatchCreatorModalComponent } from './patch-creator/patch-creator.component';

export { ActionCreatorModalComponent } from './action-creator/action-creator.component';
export { PatchCreatorModalComponent } from './patch-creator/patch-creator.component';

export const Modals = [
    ActionCreatorModalComponent,
    PatchCreatorModalComponent
];
