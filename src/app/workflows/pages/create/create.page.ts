import camelCase from 'camelcase';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import slugify from 'slugify';
import { pathOr } from 'ramda';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { WorkflowService } from '../../../../lib/store';
import { ActionCreatorModalComponent, PatchCreatorModalComponent } from '../../modals';
import { propOr, omit } from 'ramda';

@Component({
    templateUrl: './create.page.html'
})
export class CreatePageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public contentFields: any[];
    public openFields = {};

    constructor(
        private workflowService: WorkflowService,
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
        private router: Router,
        private toastr: ToastrService
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required],
            slug: ['', Validators.required],
            initial: ['', Validators.required],
            states: this.formBuilder.array([])
        });

        this.form.get('name').valueChanges
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe((value) => this.form.patchValue({
                slug: slugify(value).toLowerCase()
            }));
    }

    private buildStateField(existingValues?: any) {
        return this.formBuilder.group({
            on: this.formBuilder.control({}),
            meta: this.formBuilder.control({}),
            name: [''],
            slug: ['']
        });
    }

    public addAction(e: Event, index: number) {
        e.preventDefault();

        const dialogRef = this.dialog.open(ActionCreatorModalComponent, {
            data: {
                states: this.form.get('states').value
            }
        });

        dialogRef.afterClosed()
            .pipe(
                first()
            ).subscribe((modalResult) => {
                if (!modalResult) {
                    return;
                }

                const existingActionConfig = (this.form.get('states') as FormArray).at(index).get('on').value;
                (this.form.get('states') as FormArray).at(index).get('on').setValue({
                    ...existingActionConfig,
                    [modalResult.action]: modalResult.newState
                });
            });
    }

    public addPatch(e: Event, index: number) {
        e.preventDefault();

        const dialogRef = this.dialog.open(PatchCreatorModalComponent);

        dialogRef.afterClosed()
            .pipe(
                first()
            ).subscribe((modalResult) => {
                if (!modalResult) {
                    return;
                }

                const existingMeta = (this.form.get('states') as FormArray).at(index).get('meta').value;
                (this.form.get('states') as FormArray).at(index).get('meta').setValue({
                    ...existingMeta,
                    patches: [
                        ...existingMeta.patches || [],
                        modalResult
                    ]
                });
            });
    }

    public toggleFieldExpansion(e: Event, i: number) {
        e.preventDefault();
        this.openFields[i] = !pathOr(false, [i])(this.openFields);
    }

    public removeAction(e: Event, index: number, action: string) {
        e.preventDefault();
        const values = (this.form.get('states') as FormArray).at(index).get('on').value;
        (this.form.get('states') as FormArray).at(index).get('on').setValue(omit([action])(values));
    }

    public removePatch(e: Event, index: number, patchIndex: number) {
        e.preventDefault();
        const meta = (this.form.get('states') as FormArray).at(index).get('meta').value;
        const patches = JSON.parse(JSON.stringify(propOr([], 'patches')(meta)));
        patches.splice(patchIndex, 1);
        (this.form.get('states') as FormArray).at(index).get('meta').setValue({
            ...meta,
            patches,
        });
    }

    public removeState(e: Event, index: number) {
        e.preventDefault();
        (this.form.get('states') as FormArray).removeAt(index);
    }

    public getStates() {
        return this.form.get('states').value.map((state) => ({
            value: state.slug,
            label: state.name
        }));
    }

    public addState(e: Event) {
        e.preventDefault();

        const newState = this.buildStateField();

        newState.get('name').valueChanges
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe((value) => newState.patchValue({
                slug: slugify(value).toLowerCase()
            }));

        (this.form.get('states') as FormArray).push(newState);
    }

    public handleDrop(e) {}

    public submit(e: Event) {
        e.preventDefault();
        this.workflowService.create(this.form.value)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('Worfklow created', 'Success');
                this.router.navigate(['/tenant/workflows', result.uuid]);
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
