import camelCase from 'camelcase';
import { ToastrService } from 'ngx-toastr';
import { pathOr, propOr, omit } from 'ramda';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import slugify from 'slugify';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { WorkflowService, WorkflowQuery } from '../../../../lib/store';
import { ActionCreatorModalComponent, PatchCreatorModalComponent } from '../../modals';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public contentFields: any[];
    public openFields = {};

    constructor(
        private workflowService: WorkflowService,
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required],
            slug: ['', Validators.required],
            initial: ['', Validators.required],
            states: this.formBuilder.array([])
        });

        this.workflowService.fetchOne(this.activatedRoute.snapshot.params.id)
            .pipe(
                first()
            )
            .subscribe((workflow) => {
                this.form.patchValue(workflow);

                (workflow.states || []).map((state) => {
                    (this.form.get('states') as FormArray).push(this.buildStateField(state));
                });
            });
    }

    public toggleFieldExpansion(e: Event, i: number) {
        e.preventDefault();
        this.openFields[i] = !pathOr(false, [i])(this.openFields);
    }

    public addAction(e: Event, index: number) {
        e.preventDefault();

        const dialogRef = this.dialog.open(ActionCreatorModalComponent, {
            data: {
                states: this.form.get('states').value
            }
        });

        dialogRef.afterClosed()
            .pipe(
                first()
            ).subscribe((modalResult) => {
                if (!modalResult) {
                    return;
                }

                const existingActionConfig = (this.form.get('states') as FormArray).at(index).get('on').value;
                (this.form.get('states') as FormArray).at(index).get('on').setValue({
                    ...existingActionConfig,
                    [modalResult.action]: modalResult.newState
                });
            });
    }

    public addPatch(e: Event, index: number) {
        e.preventDefault();

        const dialogRef = this.dialog.open(PatchCreatorModalComponent);

        dialogRef.afterClosed()
            .pipe(
                first()
            ).subscribe((modalResult) => {
                if (!modalResult) {
                    return;
                }

                const existingMeta = (this.form.get('states') as FormArray).at(index).get('meta').value;
                (this.form.get('states') as FormArray).at(index).get('meta').setValue({
                    ...existingMeta,
                    patches: [
                        ...existingMeta.patches || [],
                        modalResult
                    ]
                });
            });
    }

    public removeAction(e: Event, index: number, action: string) {
        e.preventDefault();
        const values = (this.form.get('states') as FormArray).at(index).get('on').value;
        (this.form.get('states') as FormArray).at(index).get('on').setValue(omit([action])(values));
    }

    public removePatch(e: Event, index: number, patchIndex: number) {
        e.preventDefault();
        const meta = (this.form.get('states') as FormArray).at(index).get('meta').value;
        const patches = JSON.parse(JSON.stringify(propOr([], 'patches')(meta)));
        patches.splice(patchIndex, 1);
        (this.form.get('states') as FormArray).at(index).get('meta').setValue({
            ...meta,
            patches,
        });
    }

    public removeState(e: Event, index: number) {
        e.preventDefault();
        (this.form.get('states') as FormArray).removeAt(index);
    }

    public getStates() {
        return this.form.get('states').value.map((state) => ({
            value: state.slug,
            label: state.name
        }));
    }

    public addState(e: Event) {
        e.preventDefault();

        const newState = this.buildStateField();

        newState.get('name').valueChanges
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe((value) => newState.patchValue({
                slug: slugify(value).toLowerCase()
            }));

        (this.form.get('states') as FormArray).push(newState);
    }

    private buildStateField(existingValues?: any) {
        return this.formBuilder.group({
            on: [propOr({}, 'on')(existingValues)],
            meta: [propOr({}, 'meta')(existingValues)],
            name: [propOr('', 'name')(existingValues)],
            slug: [propOr('', 'slug')(existingValues)]
        });
    }

    public submit(e: Event) {
        e.preventDefault();
        this.workflowService.update(this.activatedRoute.snapshot.params.id, this.form.value)
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.success('Workflow updated', 'Success');
            });
    }

    public delete(e: Event) {
        e.preventDefault();
        this.workflowService.delete(this.activatedRoute.snapshot.params.id)
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.warning('Workflow deleted', 'Success');
                this.router.navigate(['/tenant/workflows']);
            });
    }

    public handleDrop(e) {}

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
