import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';

import { WorkflowQuery, WorkflowService } from '../../../../lib/store';

@Component({
    templateUrl: './list.page.html'
})
export class ListPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();
    public workflows$;

    constructor(
        private workflowService: WorkflowService,
        private workflowQuery: WorkflowQuery
    ) { }

    public ngOnInit(): void {
        this.workflows$ = this.workflowQuery.selectAll();
        this.workflowService.fetch()
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe();
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
