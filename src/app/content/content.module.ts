import { MomentModule } from 'ngx-moment';
import { ToastrModule } from 'ngx-toastr';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';

import { UiModule } from '../../lib/ui/ui.module';
import { Components } from './components';
import { ContentRoutingModule } from './content-routing.module';
import { Modals } from './modals';
import { Pages } from './pages';
import { Queries, Stores, StoreServices } from './store';

@NgModule({
    declarations: [
        Components,
        Modals,
        Pages
    ],
    imports: [
        CommonModule,
        ContentRoutingModule,
        RouterModule,
        UiModule,
        ReactiveFormsModule,
        ToastrModule,
        MatDialogModule,
        MomentModule,
    ],
    providers: [
        Queries,
        Stores,
        StoreServices
    ],
    entryComponents: [
        Modals,
    ]
})
export class ContentModule { }
