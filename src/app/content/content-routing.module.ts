import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import * as Pages from './pages';

const routes: Routes = [
    {
        path: ':contentTypeSlug',
        component: Pages.ListPageComponent
    },
    {
        path: ':contentTypeSlug/create',
        component: Pages.CreatePageComponent
    },
    {
        path: ':contentTypeSlug/:contentId',
        component: Pages.DetailPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ContentRoutingModule { }
