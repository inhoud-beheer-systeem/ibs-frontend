import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ContentTypeQuery } from '../../../core/store';
import { ContentQuery, ContentService } from '../../store';

@Component({
    templateUrl: './list.page.html'
})
export class ListPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public content$: Observable<any>;
    public contentType$: Observable<any>;

    constructor(
        private contentService: ContentService,
        private contentQuery: ContentQuery,
        private contentTypeQuery: ContentTypeQuery,
        private activatedRoute: ActivatedRoute
    ) { }

    public ngOnInit(): void {
        this.content$ = this.contentQuery.selectAll();
        this.activatedRoute.params.subscribe(() => {
            this.fetchContent();
        });

        this.fetchContent();
    }

    public fetchContent() {
        this.contentType$ = this.contentTypeQuery.selectEntity(this.activatedRoute.snapshot.params.contentTypeSlug);
        this.contentService.fetch(this.activatedRoute.snapshot.params.contentTypeSlug, null, {
            'sort[updatedAt]': 'DESC'
        })
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe();
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
