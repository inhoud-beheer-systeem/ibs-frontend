import { ToastrService } from 'ngx-toastr';
import { pathOr } from 'ramda';
import { combineLatest, Subject, Observable } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';
import slugify from 'slugify';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import {
    ContentFieldQuery, ContentFieldService, ContentTypeQuery
} from '../../../core/store';
import { ContentService } from '../../store';
import { LanguageQuery, LanguageService } from '../../../../lib/store';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public contentType: any;
    public content: any;
    public language: string;
    public languages$: Observable<any[]>;

    constructor(
        private contentTypeQuery: ContentTypeQuery,
        private contentFieldService: ContentFieldService,
        private contentFieldQuery: ContentFieldQuery,
        private contentService: ContentService,
        private languageQuery: LanguageQuery,
        private languageService: LanguageService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
    ) { }

    public ngOnInit(): void {
        this.contentFieldService.fetchContentFields()
            .pipe(
                first()
            ).subscribe();

        this.languageService.fetchActive()
            .pipe(
                first()
            ).subscribe((languages) => this.language = pathOr(null, ['_embedded', 0, 'key'])(languages));

        this.languages$ = this.languageQuery.selectAll();
        this.fetch();
    }

    public fetch() {
        combineLatest(
            this.contentService.fetchOne(
                this.activatedRoute.snapshot.params.contentTypeSlug,
                this.activatedRoute.snapshot.params.contentId
            ),
            this.contentTypeQuery.selectEntity(this.activatedRoute.snapshot.params.contentTypeSlug)
        )
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe(([content, contentType]) => {
                this.contentType = contentType;
                this.content = content;

                if (!contentType) {
                    return;
                }

                const fields = contentType.fields.reduce((acc, field) => ({
                    ...acc,
                    [field.slug]: ['']
                }), {});

                this.form = this.formBuilder.group({
                    name: ['', Validators.required],
                    slug: ['', Validators.required],
                    fields: this.formBuilder.group(fields)
                });

                // this.form.get('name').valueChanges
                //     .pipe(
                //         takeUntil(this.componentDestroyed$)
                //     ).subscribe((value) => this.form.patchValue({
                //         slug: slugify(value).toLowerCase()
                //     }));

                this.form.patchValue(content);
            });
    }

    public setLanguage(key: string): void {
        this.language = key;
    }

    public submit(e: Event, action: string) {
        e.preventDefault();
        this.contentService.update(
            this.activatedRoute.snapshot.params.contentTypeSlug,
            this.activatedRoute.snapshot.params.contentId,
            this.form.value,
            action
        )
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.success('Content item updated', 'Success');

                // TODO: just use the PUT request respone
                this.fetch();
            });
    }

    public getFieldConfig(contentField: any) {
        const value = this.contentFieldQuery.getValue() as any;
        const fieldType = value.result.find((field) => field.identifier === contentField.fieldType);

        return pathOr({}, ['config'])(fieldType);
    }

    public delete(e: Event) {
        e.preventDefault();
        this.contentService.delete(
            this.activatedRoute.snapshot.params.contentTypeSlug,
            this.activatedRoute.snapshot.params.contentId
        )
            .pipe(
                first()
            ).subscribe(() => {
                this.router.navigate(['../'], {
                    relativeTo: this.activatedRoute
                });
                this.toastr.warning('Content item deleted', 'Success');
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
