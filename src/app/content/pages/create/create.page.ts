import { ToastrService } from 'ngx-toastr';
import { Subject, Observable } from 'rxjs';
import { pathOr } from 'ramda';
import { first, takeUntil, tap } from 'rxjs/operators';
import slugify from 'slugify';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ContentTypeQuery } from '../../../core/store';
import { ContentService } from '../../store';
import { LanguageService, LanguageQuery } from '../../../../lib/store';
import { path } from 'ramda';

@Component({
    templateUrl: './create.page.html'
})
export class CreatePageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public language: string;
    public form: FormGroup;
    public contentType$: any;
    public languages$: Observable<any[]>;

    constructor(
        private contentTypeQuery: ContentTypeQuery,
        private contentService: ContentService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private languageService: LanguageService,
        private languageQuery: LanguageQuery,
        private router: Router,
        private toastr: ToastrService,
    ) { }

    public ngOnInit(): void {
        this.languageService.fetchActive()
            .pipe(
                first()
            ).subscribe((languages) => this.language = pathOr(null, ['_embedded', 0, 'key'])(languages));

        this.languages$ = this.languageQuery.selectAll();

        this.contentType$ = this.contentTypeQuery.selectEntity(this.activatedRoute.snapshot.params.contentTypeSlug)
            .pipe(
                tap((contentType) => {
                    if (!contentType) {
                        return;
                    }

                    const fields = contentType.fields.reduce((acc, field) => ({
                        ...acc,
                        [field.slug]: ['']
                    }), {});

                    this.form = this.formBuilder.group({
                        name: ['', Validators.required],
                        slug: ['', Validators.required],
                        state: [path(['workflow', 'initial'])(contentType), Validators.required],
                        fields: this.formBuilder.group(fields)
                    });

                    this.form.get('name').valueChanges
                        .pipe(
                            takeUntil(this.componentDestroyed$)
                        ).subscribe((value) => this.form.patchValue({
                            slug: slugify(value).toLowerCase()
                        }));
                })
            );
    }

    public setLanguage(key: string): void {
        this.language = key;
    }

    public submit(e: Event) {
        e.preventDefault();
        this.contentService.create(this.activatedRoute.snapshot.params.contentTypeSlug, this.form.value)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('Content item created', 'Success');
                this.router.navigate(['../', result.uuid], {
                    relativeTo: this.activatedRoute
                });
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
