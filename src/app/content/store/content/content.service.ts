import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ContentStore } from './content.store';

@Injectable()
export class ContentService {

    constructor(
        private contentStore: ContentStore,
        private http: HttpClient
    ) { }

    create(contentTypeId: string, values) {
        this.contentStore.setLoading(true);
        return this.http.post<any>(`/api/v1/content/${contentTypeId}`, values)
            .pipe(
                tap(result => {
                    this.contentStore.add(result as any);
                    this.contentStore.setLoading(false);
                })
            );
    }

    fetch(contentTypeId: string, filters: any, sort: any) {
        this.contentStore.setLoading(true);
        return this.http.get<any>(`/api/v1/content/${contentTypeId}?limit=500`, {
            params: {
                ...(sort && sort),
                ...(filters && filters),
                showUnpublished: true,
            }
        })
            .pipe(
                    tap(result => {
                    this.contentStore.set(result._embedded);
                    this.contentStore.setLoading(false);
                })
            );
    }

    fetchOne(contentTypeId: string, contentId: string) {
        this.contentStore.setLoading(true);
        return this.http.get<any>(`/api/v1/content/${contentTypeId}/${contentId}`)
            .pipe(
                tap(result => {
                    this.contentStore.set([result as any]);
                    this.contentStore.setLoading(false);
                })
            );
    }

    update(contentTypeId: string, contentId: string, values: any, action: string) {
        this.contentStore.setLoading(true);
        return this.http.put<any>(`/api/v1/content/${contentTypeId}/${contentId}`, values, {
            params: {
                action
            }
        })
            .pipe(
                tap(() => {
                    this.contentStore.set([values]);
                    this.contentStore.setLoading(false);
                })
            );
    }

    delete(contentTypeId: string, contentId: string) {
        this.contentStore.setLoading(true);
        return this.http.delete<any>(`/api/v1/content/${contentTypeId}/${contentId}`)
            .pipe(
                tap(result => {
                    this.contentStore.remove(contentId);
                    this.contentStore.setLoading(false);
                })
            );
    }
}
