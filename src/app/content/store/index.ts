import { ContentQuery } from './content/content.query';
import { ContentService } from './content/content.service';
import { ContentStore } from './content/content.store';

export { ContentQuery } from './content/content.query';
export { ContentService } from './content/content.service';
export { ContentStore } from './content/content.store';

export const StoreServices = [
    ContentService
];

export const Stores = [
    ContentStore
];

export const Queries = [
    ContentQuery
];

