import { MomentModule } from 'ngx-moment';

import { DragulaModule } from 'ng2-dragula';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';

import { UiModule } from '../../lib/ui/ui.module';
import { Components } from './components';
import { ContentTypesRoutingModule } from './content-types-routing.module';
import { Modals } from './modals';
import { Pages } from './pages';

@NgModule({
    declarations: [
        Components,
        Modals,
        Pages
    ],
    imports: [
        DragulaModule,
        CommonModule,
        ContentTypesRoutingModule,
        RouterModule,
        UiModule,
        ReactiveFormsModule,
        MomentModule,
        MatDialogModule,
    ],
    entryComponents: [
        Modals,
    ]
})
export class ContentTypesModule { }
