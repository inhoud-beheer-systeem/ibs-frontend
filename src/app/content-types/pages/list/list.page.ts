import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';

import { ContentTypeQuery, ContentTypeService } from '../../../core/store';

@Component({
    templateUrl: './list.page.html'
})
export class ListPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();
    public contentTypes$;

    constructor(
        private contentTypeService: ContentTypeService,
        private contentTypeQuery: ContentTypeQuery
    ) { }

    public ngOnInit(): void {
        this.contentTypes$ = this.contentTypeQuery.selectAll();
        this.contentTypeService.fetch()
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe();
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
