import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ContentFieldService, ContentTypeService } from '../../../core/store';
import { WorkflowService } from '../../../../lib/store';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public formValues: any;
    public contentFields: any[];
    public contentType: any;
    public workflows = [];
    public openFields = {};

    constructor(
        private contentTypeService: ContentTypeService,
        private contentFieldService: ContentFieldService,
        private activatedRoute: ActivatedRoute,
        private workflowService: WorkflowService,
        private router: Router,
        private toastr: ToastrService,
    ) { }

    public ngOnInit(): void {
        this.workflowService.fetch()
            .pipe(
                first()
            ).subscribe((workflows) => {
                this.workflows = workflows._embedded.map((workflow) => ({
                    value: workflow.uuid,
                    label: workflow.name
                }));
            });

        this.contentFieldService.fetchContentFields()
            .pipe(
                first()
            ).subscribe((contentFields) => {
                this.contentFields = contentFields as any;
                this.contentTypeService.fetchOne(this.activatedRoute.snapshot.params.id)
                    .pipe(
                        first()
                    ).subscribe((contentType) => {
                        this.formValues = contentType;
                        this.contentType = contentType;
                    });
            });
    }

    public submit(e: Event) {
        e.preventDefault();
        this.contentTypeService.update(this.activatedRoute.snapshot.params.id, this.formValues)
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.success('Content type updated', 'Success');
            });
    }

    public handleFormChange(values) {
        this.formValues = values;
    }

    public delete(e: Event) {
        e.preventDefault();
        this.contentTypeService.delete(this.activatedRoute.snapshot.params.id)
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.warning('Content type deleted', 'Success');
                this.router.navigate(['/content-types']);
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
