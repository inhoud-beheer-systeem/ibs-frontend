import { ToastrService } from 'ngx-toastr';
import { combineLatest, Subject } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TenantService } from '../../store';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public tenant: any;

    constructor(
        private tenantService: TenantService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            slug: ['', Validators.required],
            url: ['', Validators.required],
            storage: this.formBuilder.group({
                type: ['ftp'],
                config: this.formBuilder.group({
                    host: ['myftpserver.com'],
                    user: ['usrname'],
                    password: ['password'],
                    secure: [true]
                })
            }),
            mail: this.formBuilder.group({
                type: ['smtp'],
                config: this.formBuilder.group({
                    host: [''],
                    port: [465],
                    secure: [true],
                    user: [''],
                    password: [''],
                })
            })
        });

        this.tenantService.fetchOne(this.activatedRoute.snapshot.params.id)
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe((tenant) => {
                this.form.patchValue(tenant);
            });
    }

    public submit(e: Event) {
        e.preventDefault();
        this.tenantService.update(
            this.activatedRoute.snapshot.params.id,
            this.form.value
        )
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.success('Tenant updated', 'Success');
            });
    }

    public delete(e: Event) {
        e.preventDefault();
        this.tenantService.delete(
            this.activatedRoute.snapshot.params.id
        )
            .pipe(
                first()
            ).subscribe(() => {
                this.router.navigate(['../'], {
                    relativeTo: this.activatedRoute
                });
                this.toastr.warning('Tenant deleted', 'Success');
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
