import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TenantService } from '../../store';

@Component({
    templateUrl: './create.page.html'
})
export class CreatePageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public contentType$: any;

    constructor(
        private tenantService: TenantService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            slug: ['', Validators.required],
            url: ['', Validators.required],
        });
    }

    public submit(e: Event) {
        e.preventDefault();
        this.tenantService.create(this.form.value)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('Tenant created', 'Success');
                this.router.navigate(['../', result.uuid], {
                    relativeTo: this.activatedRoute
                });
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
