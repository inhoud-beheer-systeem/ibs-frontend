import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TenantQuery, TenantService } from '../../store';

@Component({
    templateUrl: './list.page.html'
})
export class ListPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public content$: Observable<any>;

    constructor(
        private tentantService: TenantService,
        private tentantQuery: TenantQuery,
        private activatedRoute: ActivatedRoute
    ) { }

    public ngOnInit(): void {
        this.content$ = this.tentantQuery.selectAll();
        this.activatedRoute.params.subscribe(() => {
            this.fetchContent();
        });

        this.fetchContent();
    }

    public fetchContent() {
        this.tentantService.fetch()
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe();
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
