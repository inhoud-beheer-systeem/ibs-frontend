import { Injectable } from '@angular/core';
import { EntityState, EntityStore, ID, StoreConfig } from '@datorama/akita';

export interface Tenant {
    uuid: string;
    key: string;
    label: string;
    tenantUuid: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface TenantState extends EntityState<Tenant> { }

@Injectable()
@StoreConfig({ name: 'tenants', idKey: 'uuid' })
export class TenantStore extends EntityStore<TenantState, Tenant> {
  constructor() {
    super();
  }
}
