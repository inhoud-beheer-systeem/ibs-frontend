import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { TenantStore } from './tenants.store';

@Injectable()
export class TenantService {

    constructor(
        private tenantStore: TenantStore,
        private http: HttpClient
    ) { }

    create(values) {
        this.tenantStore.setLoading(true);
        return this.http.post<any>('/api/v1/tenants', values)
            .pipe(
                tap(result => {
                    this.tenantStore.add(result);
                    this.tenantStore.setLoading(false);
                })
            );
    }

    fetch() {
        this.tenantStore.setLoading(true);
        return this.http.get<any>('/api/v1/tenants' )
            .pipe(
                tap(result => {
                    this.tenantStore.set(result._embedded);
                    this.tenantStore.setLoading(false);
                })
            );
    }

    fetchOne(id) {
        this.tenantStore.setLoading(true);
        return this.http.get<any>(`/api/v1/tenants/${id}`)
            .pipe(
                tap(result => {
                    this.tenantStore.update([result]);
                    this.tenantStore.setLoading(false);
                })
            );
    }

    update(id: string, values: any) {
        this.tenantStore.setLoading(true);
        return this.http.put<any>(`/api/v1/tenants/${id}`, values)
            .pipe(
                tap((result) => {
                    this.tenantStore.update(result);
                    this.tenantStore.setLoading(false);
                })
            );
    }

    delete(id: string) {
        this.tenantStore.setLoading(true);
        return this.http.delete<any>(`/api/v1/tenants/${id}`)
            .pipe(
                tap(() => {
                    this.tenantStore.remove(id);
                    this.tenantStore.setLoading(false);
                })
            );
    }
}
