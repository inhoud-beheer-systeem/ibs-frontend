import { TenantQuery } from './tenants/tenants.query';
import { TenantService } from './tenants/tenants.service';
import { TenantStore } from './tenants/tenants.store';

export { TenantQuery } from './tenants/tenants.query';
export { TenantService } from './tenants/tenants.service';
export { TenantStore } from './tenants/tenants.store';

export const StoreServices = [
    TenantService
];

export const Stores = [
    TenantStore
];

export const Queries = [
    TenantQuery
];

