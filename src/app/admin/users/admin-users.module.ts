import { MomentModule } from 'ngx-moment';
import { ToastrModule } from 'ngx-toastr';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';

import { UiModule } from '../../../lib/ui/ui.module';
import { AdminUsersRoutingModule } from './admin-users-routing.module';
import { Pages } from './pages';
import { Queries, Stores, StoreServices } from './store';

@NgModule({
    declarations: [
        Pages
    ],
    imports: [
        CommonModule,
        AdminUsersRoutingModule,
        RouterModule,
        UiModule,
        ReactiveFormsModule,
        ToastrModule,
        MatDialogModule,
        MomentModule,
    ],
    providers: [
        Queries,
        Stores,
        StoreServices
    ],
})
export class AdminUsersModule { }
