import { UserQuery } from './users/users.query';
import { UserService } from './users/users.service';
import { UserStore } from './users/users.store';

export { UserQuery } from './users/users.query';
export { UserService } from './users/users.service';
export { UserStore } from './users/users.store';

export const StoreServices = [
    UserService
];

export const Stores = [
    UserStore
];

export const Queries = [
    UserQuery
];

