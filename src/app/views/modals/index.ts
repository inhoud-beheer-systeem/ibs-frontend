import {
    ContentFieldSelectorModalComponent
} from './content-field-selector/content-field-selector.component';

export {
    ContentFieldSelectorModalComponent
} from './content-field-selector/content-field-selector.component';

export const Modals = [
    ContentFieldSelectorModalComponent
];
