import { ViewPreviewService } from './view-preview.service';

export { ViewPreviewService } from './view-preview.service';

export const Services = [
    ViewPreviewService
];
