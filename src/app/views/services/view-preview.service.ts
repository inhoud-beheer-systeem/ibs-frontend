import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ViewPreviewService {
    constructor(
        private http: HttpClient
    ) { }

    public fetchPreview(configuration): any {
        return this.http.post('/api/views/preview', configuration);
    }
}
