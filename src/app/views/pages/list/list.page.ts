import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';

import { ViewQuery, ViewService } from '../../../../lib/store';

@Component({
    templateUrl: './list.page.html'
})
export class ListPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();
    public views$;

    constructor(
        private viewService: ViewService,
        private viewQuery: ViewQuery
    ) { }

    public ngOnInit(): void {
        this.views$ = this.viewQuery.selectAll();
        this.viewService.fetch()
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe();
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
