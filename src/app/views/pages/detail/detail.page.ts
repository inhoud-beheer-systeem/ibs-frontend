import { ToastrService } from 'ngx-toastr';
import { path, pathOr } from 'ramda';
import { config, Observable, Subject } from 'rxjs';
import { debounce, debounceTime, first, map, takeUntil } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ViewService } from '../../../../lib/store';
import { ContentTypeQuery, ContentTypeService } from '../../../core/store';
import { ViewPreviewService } from '../../services';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public contentTypes$: Observable<any>;
    public preview: any[] = [];
    public selectedContentType: any;
    public form: FormGroup;
    public contentFields: any[];
    public contentFormControl = new FormControl(null);

    constructor(
        private viewService: ViewService,
        private contentTypeService: ContentTypeService,
        private contentTypeQuery: ContentTypeQuery,
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private toastr: ToastrService,
        private viewPreciewService: ViewPreviewService
    ) { }

    public ngOnInit(): void {
        this.contentTypes$ = this.contentTypeService.fetch()
            .pipe(
                map(({ _embedded }) => {
                    return _embedded.map((contentType) => ({
                        value: contentType.slug,
                        label: contentType.name
                    }));
                }),
                takeUntil(this.componentDestroyed$)
            );

        this.viewService.fetchOne(this.activatedRoute.snapshot.params.id)
            .pipe(first())
            .subscribe((view) => {
                this.selectedContentType = view.contentType;
                view.configuration.conditions.map((condition) => {
                    (this.form.get('configuration.conditions') as FormArray).push(this.buildConditionForm(condition));
                });
                this.form.patchValue({
                    ...view,
                    contentType: view.contentType.slug
                }, {
                    emitEvent: false,
                });
            });

        this.buildForm();
    }

    public addContentItem(contentUuid: string) {
        (this.form.get('configuration.conditions') as FormArray).push(this.buildConditionForm({
            field: 'uuid',
            operator: '=',
            value: contentUuid
        }));
    }

    public buildForm() {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required],
            contentType: ['', Validators.required],
            viewType: ['static', Validators.required],
            configuration: this.buildConfigurationForm()
        });

        this.form.get('contentType').valueChanges
            .pipe(
                takeUntil(this.componentDestroyed$)
            )
            .subscribe((contentTypeSlug: string) => {
                this.selectedContentType = this.contentTypeQuery.getEntity(contentTypeSlug);

                this.form.patchValue({
                    configuration: {
                        order: {
                            column: path(['fields', 0, 'slug'])(this.selectedContentType)
                        }
                    }
                });
            });

        this.form.get('configuration').valueChanges
            .pipe(
                takeUntil(this.componentDestroyed$),
                debounceTime(500)
            )
            .subscribe((configuration) => {
                this.viewPreciewService.fetchPreview({
                    contentType: this.selectedContentType.uuid,
                    viewType: this.form.get('viewType').value,
                    configuration,
                })
                    .pipe(
                        first()
                    )
                    .subscribe((preview) => this.preview = preview as any);
            });
    }

    public removeCondition(index: number) {
        (this.form.get('configuration.conditions') as FormArray).removeAt(index);
    }

    public addCondition() {
        (this.form.get('configuration.conditions') as FormArray).push(this.buildConditionForm());
    }

    public handleDrop(newArray) {
        (this.form.get('configuration.conditions') as FormArray).setValue(newArray);
    }

    public findInPreview(contentUuid: string) {
        return this.preview.find((item) => {
            return item.uuid === contentUuid;
        });
    }

    public buildConfigurationForm(): FormGroup {
        return this.formBuilder.group({
            conditions: this.formBuilder.array([]),
            order: this.formBuilder.group({
                column: [''],
                direction: ['ASC']
            })
        });
    }

    public buildConditionForm(condition = {}): FormGroup {
        return this.formBuilder.group({
            field: [pathOr(null, ['field'])(condition), Validators.required],
            operator: [pathOr('-', ['operator'])(condition), Validators.required],
            value: [pathOr(null, ['value'])(condition), Validators.required],
        });
    }

    public getColumns(): any[] {
        return pathOr([], ['fields'])(this.selectedContentType).map((field) => ({
            value: field.slug,
            label: field.name,
        }));
    }

    public findField(slug: string): any {
        return pathOr([], ['fields'])(this.selectedContentType).find((field) => field.slug === slug);
    }

    public submit(e: Event) {
        e.preventDefault();
        this.viewService.update(this.activatedRoute.snapshot.params.id, this.form.value)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('View created', 'Success');
                this.router.navigate(['/views', result.uuid]);
            });
    }

    public delete(e: Event) {
        e.preventDefault();
        this.viewService.delete(this.activatedRoute.snapshot.params.id)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('View deleted', 'Success');
                this.router.navigate(['/views']);
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
