import { DragulaModule } from 'ng2-dragula';
import { MomentModule } from 'ngx-moment';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';

import { UiModule } from '../../lib/ui/ui.module';
import { Components } from './components';
import { Modals } from './modals';
import { Pages } from './pages';
import { Services } from './services';
import { ViewsRoutingModule } from './views-routing.module';

@NgModule({
    declarations: [
        Components,
        Modals,
        Pages
    ],
    imports: [
        DragDropModule,
        CommonModule,
        ViewsRoutingModule,
        RouterModule,
        UiModule,
        ReactiveFormsModule,
        MomentModule,
        MatDialogModule,
        DragulaModule
    ],
    providers: [
        Services
    ],
    entryComponents: [
        Modals,
    ]
})
export class ViewsModule { }
