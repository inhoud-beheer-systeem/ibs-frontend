import { PagesQuery } from './pages/pages.query';
import { PagesService } from './pages/pages.service';
import { PagesStore } from './pages/pages.store';

export { PagesQuery } from './pages/pages.query';
export { PagesService } from './pages/pages.service';
export { PagesStore } from './pages/pages.store';

export const StoreServices = [
    PagesService
];

export const Stores = [
    PagesStore
];

export const Queries = [
    PagesQuery
];

