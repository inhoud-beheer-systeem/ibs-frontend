import { EntityState, EntityStore, ID, StoreConfig } from '@datorama/akita';
import { Injectable } from "@angular/core";

export interface Pages {
    uuid: string;
    description: string;
    fields: any[];
}

export interface PagesState extends EntityState<Pages> { }

@Injectable()
@StoreConfig({ name: 'pages', idKey: 'uuid' })
export class PagesStore extends EntityStore<PagesState, Pages> {
  constructor() {
    super();
  }
}
