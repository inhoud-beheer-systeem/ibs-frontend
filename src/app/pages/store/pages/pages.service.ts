import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PagesStore } from './pages.store';

@Injectable()
export class PagesService {

    constructor(
        private pagesStore: PagesStore,
        private http: HttpClient
    ) { }

    fetchOne(pagesTypeId: string) {
        this.pagesStore.setLoading(true);
        return this.http.get<any>(`/api/v1/pages/${pagesTypeId}`)
            .pipe(
                tap(result => {
                    this.pagesStore.set([result as any]);
                    this.pagesStore.setLoading(false);
                })
            );
    }

    update(pagesTypeId: string, values: any) {
        this.pagesStore.setLoading(true);
        return this.http.put<any>(`/api/v1/pages/${pagesTypeId}`, values)
            .pipe(
                tap(() => {
                    this.pagesStore.set([values]);
                    this.pagesStore.setLoading(false);
                })
            );
    }
}
