import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';

import { Pages, PagesState, PagesStore } from './pages.store';

@Injectable()
export class PagesQuery extends QueryEntity<PagesState, Pages> {
    constructor(protected store: PagesStore) {
        super(store);
    }
}
