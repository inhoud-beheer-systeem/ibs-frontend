import { ToastrService } from 'ngx-toastr';
import { pathOr } from 'ramda';
import { combineLatest, Subject, Observable } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';
import slugify from 'slugify';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { ContentFieldQuery, ContentFieldService, PageTypeQuery } from '../../../core/store';
import { PagesService } from '../../store';
import { LanguageQuery, LanguageService } from '../../../../lib/store';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public contentType: any;
    public language: string;
    public languages$: Observable<any[]>;

    constructor(
        private pageTypeQuery: PageTypeQuery,
        private contentFieldService: ContentFieldService,
        private contentFieldQuery: ContentFieldQuery,
        private pagesService: PagesService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private languageService: LanguageService,
        private languageQuery: LanguageQuery,
        private route: ActivatedRoute,
        private toastr: ToastrService,
    ) {}

    private fetch() {
        combineLatest(
            this.pagesService.fetchOne(this.activatedRoute.snapshot.params.pageTypeSlug),
            this.pageTypeQuery.selectEntity(this.activatedRoute.snapshot.params.pageTypeSlug),
        )
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe(([content, contentType]) => {
                this.contentType = contentType;

                if (!contentType) {
                    return;
                }

                const fields = contentType.fields.reduce((acc, field) => ({
                    ...acc,
                    [field.slug]: ['']
                }), {});

                this.form = this.formBuilder.group({
                    fields: this.formBuilder.group(fields)
                });

                this.form.patchValue(content);
            });
    }

    public ngOnInit(): void {
        this.languageService.fetchActive()
            .pipe(
                first()
            ).subscribe((languages) => this.language = pathOr(null, ['_embedded', 0, 'key'])(languages));

        this.languages$ = this.languageQuery.selectAll();

        this.contentFieldService.fetchContentFields()
            .pipe(
                first()
            ).subscribe();

        this.route.params.pipe(takeUntil(this.componentDestroyed$)).subscribe(() => {
            this.fetch();
        });
    }

    public submit(e: Event) {
        e.preventDefault();
        this.pagesService.update(
            this.activatedRoute.snapshot.params.pageTypeSlug,
            this.form.value
        )
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.success('Content item updated', 'Success');
            });
    }

    public setLanguage(key: string): void {
        this.language = key;
    }

    public getFieldConfig(contentField: any) {
        const value = this.contentFieldQuery.getValue() as any;
        const fieldType = value.result.find((field) => field.identifier === contentField.fieldType);

        return pathOr({}, ['config'])(fieldType);
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
