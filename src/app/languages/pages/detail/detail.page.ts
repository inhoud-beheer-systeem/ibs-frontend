import { ToastrService } from 'ngx-toastr';
import { combineLatest, Subject } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TenantLanguageService } from '../../store';

@Component({
    templateUrl: './detail.page.html'
})
export class DetailPageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public tenantLanguage: any;

    constructor(
        private tenantLanguageService: TenantLanguageService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            label: ['', Validators.required],
            key: ['', Validators.required],
        });

        this.tenantLanguageService.fetchOne(this.activatedRoute.snapshot.params.id)
            .pipe(
                takeUntil(this.componentDestroyed$)
            ).subscribe((tenantLanguage) => {
                this.form.patchValue(tenantLanguage);
            });
    }

    public submit(e: Event) {
        e.preventDefault();
        this.tenantLanguageService.update(
            this.activatedRoute.snapshot.params.id,
            this.form.value
        )
            .pipe(
                first()
            ).subscribe(() => {
                this.toastr.success('Language updated', 'Success');
            });
    }

    public delete(e: Event) {
        e.preventDefault();
        this.tenantLanguageService.delete(
            this.activatedRoute.snapshot.params.id
        )
            .pipe(
                first()
            ).subscribe(() => {
                this.router.navigate(['../'], {
                    relativeTo: this.activatedRoute
                });
                this.toastr.warning('Language deleted', 'Success');
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
