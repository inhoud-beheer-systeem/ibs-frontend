import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { first, takeUntil, tap } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TenantLanguageService } from '../../store';

@Component({
    templateUrl: './create.page.html'
})
export class CreatePageComponent implements OnInit, OnDestroy {
    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public contentType$: any;

    constructor(
        private tenantLanguageService: TenantLanguageService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
    ) { }

    public ngOnInit(): void {
        this.form = this.formBuilder.group({
            label: ['', Validators.required],
            key: ['', Validators.required],
        });
    }

    public submit(e: Event) {
        e.preventDefault();
        this.tenantLanguageService.create(this.form.value)
            .pipe(
                first()
            ).subscribe((result) => {
                this.toastr.success('Language added', 'Success');
                this.router.navigate(['../', result.uuid], {
                    relativeTo: this.activatedRoute
                });
            });
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }
}
