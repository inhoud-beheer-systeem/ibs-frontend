import { EntityState, EntityStore, ID, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';

export interface TenantLanguage {
    uuid: string;
    key: string;
    label: string;
    tenantUuid: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface TenantLanguageState extends EntityState<TenantLanguage> { }

@Injectable()
@StoreConfig({ name: 'tenant-languages', idKey: 'uuid' })
export class TenantLanguageStore extends EntityStore<TenantLanguageState, TenantLanguage> {
  constructor() {
    super();
  }
}
