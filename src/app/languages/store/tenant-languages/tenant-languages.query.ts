import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';

import { TenantLanguage, TenantLanguageState, TenantLanguageStore } from './tenant-languages.store';

@Injectable()
export class TenantLanguageQuery extends QueryEntity<TenantLanguageState, TenantLanguage> {
    constructor(protected store: TenantLanguageStore) {
        super(store);
    }
}
