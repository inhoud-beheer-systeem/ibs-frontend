import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { TenantLanguageStore } from './tenant-languages.store';

@Injectable()
export class TenantLanguageService {

    constructor(
        private tenantLanguageStore: TenantLanguageStore,
        private http: HttpClient
    ) { }

    create(values) {
        this.tenantLanguageStore.setLoading(true);
        return this.http.post<any>('/api/v1/languages', values)
            .pipe(
                tap(result => {
                    this.tenantLanguageStore.add(result);
                    this.tenantLanguageStore.setLoading(false);
                })
            );
    }

    fetch() {
        this.tenantLanguageStore.setLoading(true);
        return this.http.get<any>('/api/v1/languages' )
            .pipe(
                tap(result => {
                    this.tenantLanguageStore.set(result._embedded);
                    this.tenantLanguageStore.setLoading(false);
                })
            );
    }

    fetchOne(id) {
        this.tenantLanguageStore.setLoading(true);
        return this.http.get<any>(`/api/v1/languages/${id}`)
            .pipe(
                tap(result => {
                    this.tenantLanguageStore.update([result]);
                    this.tenantLanguageStore.setLoading(false);
                })
            );
    }

    update(id: string, values: any) {
        this.tenantLanguageStore.setLoading(true);
        return this.http.put<any>(`/api/v1/languages/${id}`, values)
            .pipe(
                tap((result) => {
                    this.tenantLanguageStore.update(result);
                    this.tenantLanguageStore.setLoading(false);
                })
            );
    }

    delete(id: string) {
        this.tenantLanguageStore.setLoading(true);
        return this.http.delete<any>(`/api/v1/languages/${id}`)
            .pipe(
                tap(() => {
                    this.tenantLanguageStore.remove(id);
                    this.tenantLanguageStore.setLoading(false);
                })
            );
    }
}
