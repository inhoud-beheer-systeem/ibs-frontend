import { TenantLanguageQuery } from './tenant-languages/tenant-languages.query';
import { TenantLanguageService } from './tenant-languages/tenant-languages.service';
import { TenantLanguageStore } from './tenant-languages/tenant-languages.store';

export { TenantLanguageQuery } from './tenant-languages/tenant-languages.query';
export { TenantLanguageService } from './tenant-languages/tenant-languages.service';
export { TenantLanguageStore } from './tenant-languages/tenant-languages.store';

export const StoreServices = [
    TenantLanguageService
];

export const Stores = [
    TenantLanguageStore
];

export const Queries = [
    TenantLanguageQuery
];

