import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';
import { latLng, tileLayer, polygon, Map, marker, circle, FeatureGroup, polyline, Control, Icon, icon } from 'leaflet';

@Component({
    selector: 'app-map-input',
    templateUrl: './map-input.component.html',
    providers: [
        {
            provide: [],
            useExisting: forwardRef(() => MapInputComponent),
            multi: true,
        },
    ],
})
export class MapInputComponent implements OnInit, OnDestroy, ControlValueAccessor {

    constructor(public ngControl: NgControl) {
        ngControl.valueAccessor = this;
    }
    @Input() label?: string;
    @Input() placeholder = '';

    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();
    private map: Map;

    public layers = [];
    public featureGroup = new FeatureGroup();
    public control: FormControl = new FormControl('');
    public drawOptions: Control.DrawConstructorOptions = {
        position: 'topright',
        edit: {
            featureGroup: this.featureGroup,
        },
        draw: {
            rectangle: false,
            circlemarker: false,
            marker: {
                icon: icon({
                    iconSize: [ 25, 41 ],
                    iconAnchor: [ 13, 41 ],
                    iconUrl: 'assets/marker-icon.png',
                    shadowUrl: 'assets/marker-shadow.png'
                })
            }
        }
    };

    public options = {
        layers: [
            tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', { maxZoom: 18, attribution: '...' })
        ],
        zoom: 5,
        center: latLng(50.4974441, 3.3451309)
    };
    public updateValue = (_: any) => {};

    private propagateChange(value: any): void {
        if (this.updateValue && value) {
            return this.updateValue(value);
        }

        if (this.control && value) {
            this.control.setValue(value);
        }
    }

    public ngOnInit() {
        this.control.valueChanges.pipe(
            takeUntil(this.componentDestroyed$),
        ).subscribe((value) => {
            this.propagateChange(value);
        });
    }

    public handleDrawCreated(e) {
        this.handleMapControl();
    }

    public handleMapReady(map: Map) {
        this.map = map;
    }

    public handleMapControl() {
        return this.control.setValue({
            ...this.control.value,
            coordinates: this.map.getCenter(),
            zoom: this.map.getZoom(),
            layers: this.getDrawLayers(),
        });
    }

    private getDrawLayers() {
        const layers = (this.map as any)._layers;

        const polyLayers = Object.keys(layers)
            .filter((key) => layers[key]._latlngs && layers[key].editing && layers[key]._bounds)
            .map((key) => ({
                coordinates: layers[key]._latlngs,
                options: layers[key].options,
                ...( layers[key].editing._poly && layers[key]._latlngs.length === 1 && { type: 'polygon' }),
                ...( layers[key].editing._poly && layers[key]._latlngs.length > 1 && { type: 'polyline' })
            }));

        const circleLayers = Object.keys(layers)
            .filter((key) => layers[key]._latlng && layers[key].editing && layers[key]._mRadius)
            .map((key) => ({
                coordinates: layers[key]._latlng,
                options: layers[key].options,
                type: 'circle'
            }));

        const markerLayers = Object.keys(layers)
            .filter((key) => layers[key]._latlng && layers[key].editing && layers[key].editing._marker)
            .map((key) => ({
                coordinates: layers[key]._latlng,
                options: layers[key].options,
                type: 'marker'
            }));

        return polyLayers.concat(circleLayers).concat(markerLayers);
    }

    public ngOnDestroy() {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }

    public writeValue(value: any) {
        if (this.map) {
            this.map.addLayer(this.featureGroup);
        }

        if (this.map && value) {
            this.map.setView(value.coordinates, value.zoom);
            this.layers = JSON.parse(JSON.stringify(value.layers));
            value.layers.map((layer) => {
                switch (layer.type) {
                    case 'polygon':
                        this.featureGroup.addLayer(polygon(layer.coordinates, layer.options));
                        break;

                    case 'polyline':
                        this.featureGroup.addLayer(polyline(layer.coordinates, layer.options));
                        break;

                    case 'circle':
                        this.featureGroup.addLayer(circle(layer.coordinates, layer.options));
                        break;

                    case 'marker':
                        this.featureGroup.addLayer(marker(layer.coordinates, {
                            icon: new Icon({
                                iconUrl: layer.options.icon.options.iconUrl,
                                iconAnchor: layer.options.icon.options.iconAnchor,
                                iconSize: layer.options.icon.options.iconSize,
                            })
                        }));
                        break;

                    default:
                        break;
                }
            });
        }
        setTimeout(() => this.control.setValue(value));
    }

    public registerOnChange(fn: any) {
        this.updateValue = fn;
    }

    public registerOnTouched() {}
}
