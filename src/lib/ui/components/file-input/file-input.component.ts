import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormArray, FormControl, NgControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { ResourceSelectorComponent } from '../../modals';
import { DragulaService } from 'ng2-dragula';

@Component({
    selector: 'app-file-input',
    templateUrl: './file-input.component.html',
    providers: [
        {
            provide: [],
            useExisting: forwardRef(() => FileInputComponent),
            multi: true,
        },
    ],
})
export class FileInputComponent implements OnInit, OnDestroy, ControlValueAccessor {
    @Input() label?: string;
    @Input() multiple = false;
    @Input() allowedExtensions = '';

    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public control =  new FormControl('');
    public updateValue = (_: any) => {};

    constructor(
        public ngControl: NgControl,
        private dialog: MatDialog,
        private dragulaService: DragulaService,
    ) {
        ngControl.valueAccessor = this;
    }

    public ngOnInit() {
        this.control.valueChanges.pipe(
            takeUntil(this.componentDestroyed$),
        ).subscribe((value) => {
            this.propagateChange(value);
        });
    }

    public openModal() {
        const dialogRef = this.dialog.open(ResourceSelectorComponent, {
            data: {
                allowedExtensions: this.allowedExtensions.split(','),
                multiple: this.multiple,
            }
        });

        dialogRef.afterClosed()
            .pipe(
                takeUntil(this.componentDestroyed$)
            )
            .subscribe((value) => {
                if (!value) {
                    return;
                }

                if (this.multiple) {
                    return this.control.setValue([...this.control.value, ...value]);
                }

                this.control.setValue(value);
            });
    }

    private propagateChange(value: any): void {
        if (this.updateValue) {
            return this.updateValue(value);
        }

        if (this.control) {
            this.control.setValue(value);
        }
    }

    public handleDrop(images: any) {
        return this.control.setValue(images);
    }

    public removeFile(index: number) {
        const toSplice = JSON.parse(JSON.stringify(this.control.value));
        toSplice.splice(index, 1);
        return this.control.setValue(toSplice);
    }

    public ngOnDestroy() {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }

    public writeValue(value: any) {
        setTimeout(() => this.control.setValue(value));
    }

    public registerOnChange(fn: any) {
        this.updateValue = fn;
    }

    public registerOnTouched() {}
}
