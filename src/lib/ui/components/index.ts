import { BooleanInputComponent } from './boolean-input/boolean-input.component';
import { CheckboxInputComponent } from './checkbox-input/checkbox-input.component';
import { ContentInputComponent } from './content-input/content-input.component';
import { ContentTypeInputComponent } from './contenttype-input/contenttype-input.component';
import { TaxonomyInputComponent } from './taxonomy-input/taxonomy-input.component';
import { TaxonomyGroupInputComponent } from './taxonomy-group-input/taxonomy-group-input.component';
import { DynamicInputComponent } from './dynamic-input/dynamic-input.component';
import { FileInputComponent } from './file-input/file-input.component';
import { HeaderComponent } from './header/header.component';
import { IconInputComponent } from './icon-input/icon-input.component';
import { ImageInputComponent } from './image-input/image-input.component';
import { ImageTooltipComponent } from './image-tooltip/image-tooltip.component';
import { LoadingComponent } from './loading/loading.component';
import { MapInputComponent } from './map-input/map-input.component';
import { ResourceComponent } from './resource/resource.component';
import { RichTextInputComponent } from './richtext-input/richtext-input.component';
import { RoleInputComponent } from './role-input/role-input.component';
import { SelectInputComponent } from './select-input/select-input.component';
import { TablePreviewComponent } from './table-preview/table-preview.component';
import { TenantInputComponent } from './tenant-input/tenant-input.component';
import { TextInputComponent } from './text-input/text-input.component';
import { TextareaInputComponent } from './textarea-input/textarea-input.component';
import { ValidationMessageComponent } from './validation-message/validation-message.component';
import { FieldConfigComponent } from './field-config/field-config.component';
import { RepeaterConfigComponent } from './repeater-config/repeater-config.component';
import { RepeaterComponent } from './repeater/repeater.component';

export { BooleanInputComponent } from './boolean-input/boolean-input.component';
export { ContentInputComponent } from './content-input/content-input.component';
export { ContentTypeInputComponent } from './contenttype-input/contenttype-input.component';
export { DynamicInputComponent } from './dynamic-input/dynamic-input.component';
export { HeaderComponent } from './header/header.component';
export { IconInputComponent } from './icon-input/icon-input.component';
export { ImageInputComponent } from './image-input/image-input.component';
export { LoadingComponent } from './loading/loading.component';
export { ResourceComponent } from './resource/resource.component';
export { SelectInputComponent } from './select-input/select-input.component';
export { TablePreviewComponent } from './table-preview/table-preview.component';
export { TextInputComponent } from './text-input/text-input.component';
export { TextareaInputComponent } from './textarea-input/textarea-input.component';
export { RichTextInputComponent } from './richtext-input/richtext-input.component';
export { MapInputComponent } from './map-input/map-input.component';
export { FileInputComponent } from './file-input/file-input.component';
export { CheckboxInputComponent } from './checkbox-input/checkbox-input.component';
export { RoleInputComponent } from './role-input/role-input.component';
export { ImageTooltipComponent } from './image-tooltip/image-tooltip.component';
export { TenantInputComponent } from './tenant-input/tenant-input.component';
export { ValidationMessageComponent } from './validation-message/validation-message.component';
export { FieldConfigComponent } from './field-config/field-config.component';
export { RepeaterConfigComponent } from './repeater-config/repeater-config.component';
export { RepeaterComponent } from './repeater/repeater.component';
export { TaxonomyInputComponent } from './taxonomy-input/taxonomy-input.component';
export { TaxonomyGroupInputComponent } from './taxonomy-group-input/taxonomy-group-input.component';

export const Components = [
    MapInputComponent,
    HeaderComponent,
    LoadingComponent,
    TextInputComponent,
    DynamicInputComponent,
    BooleanInputComponent,
    TextareaInputComponent,
    IconInputComponent,
    ImageInputComponent,
    ResourceComponent,
    TablePreviewComponent,
    SelectInputComponent,
    ContentInputComponent,
    ContentTypeInputComponent,
    RichTextInputComponent,
    FileInputComponent,
    CheckboxInputComponent,
    RoleInputComponent,
    ImageTooltipComponent,
    TenantInputComponent,
    ValidationMessageComponent,
    FieldConfigComponent,
    RepeaterConfigComponent,
    RepeaterComponent,
    TaxonomyInputComponent,
    TaxonomyGroupInputComponent
];
