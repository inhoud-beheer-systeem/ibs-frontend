import { path, pathOr, prop } from 'ramda';
import { Observable, Subject } from 'rxjs';
import { first, map, takeUntil } from 'rxjs/operators';

import { Component, forwardRef, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';

import { DynamicInputService } from '../../services';

@Component({
    selector: 'app-contenttype-input',
    templateUrl: './contenttype-input.component.html',
    providers: [
        {
            provide: [],
            useExisting: forwardRef(() => ContentTypeInputComponent),
            multi: true,
        },
    ],
})
export class ContentTypeInputComponent implements OnInit, OnDestroy, ControlValueAccessor, OnChanges {
    @Input() label?: string;
    @Input() placeholder = '';
    @Input() multiple = false;
    @Input() options = [];
    @Input() contentType: string;

    private componentDestroyed$: Subject<boolean> = new Subject<boolean>();

    public data: string;
    public control: FormControl = new FormControl('');
    public updateValue = (_: any) => { };

    constructor(
        public ngControl: NgControl,
        private dynamicInputService: DynamicInputService
    ) {
        ngControl.valueAccessor = this;
    }

    public ngOnInit() {
        this.doSearch();
        this.control.valueChanges.pipe(
            takeUntil(this.componentDestroyed$),
        ).subscribe((value) => {
            if (!this.multiple) {
                return this.propagateChange(pathOr(value, ['value'])(value));
            }

            this.propagateChange((value || []).map((contentItem) => pathOr(contentItem, ['value'])(contentItem)));
        });
    }

    private propagateChange(value: any): void {
        if (this.updateValue) {
            return this.updateValue(value);
        }

        if (this.control) {
            this.control.setValue(value);
        }
    }

    public ngOnChanges(changes): void {
        if (path(['contentType'])(changes)) {
            this.doSearch();
        }
    }

    public handleSearch(value) {
        // TODO: add debounce
        this.doSearch(value.term);
    }

    public doSearch(searchString = '') {
        this.dynamicInputService.fetchContentType({ name: searchString })
            .pipe(
                first(),
                map((content) => {
                    return content.map((item) => ({
                        value: item.slug,
                        label: item.name
                    }));
                })
            )
            .subscribe(data => this.data = data);
    }

    public ngOnDestroy() {
        this.componentDestroyed$.next(true);
        this.componentDestroyed$.complete();
    }

    public writeValue(value: any) {
        if (!value) {
            return;
        }

        this.dynamicInputService.fetchContentType()
            .pipe(first())
            .subscribe(data => {
                if (!this.multiple) {
                    return setTimeout(() => this.control.setValue({
                        value,
                        label: prop('name')(data.find((contentType) => contentType.slug === value))
                    }));
                }

                const mappedValue = value.map((item) => ({
                    value: item,
                    label: prop('name')(data.find((contentType) => contentType.slug === item))
                }));
                this.control.setValue(mappedValue);
            });
    }

    public registerOnChange(fn: any) {
        this.updateValue = fn;
    }

    public registerOnTouched() {}
}
