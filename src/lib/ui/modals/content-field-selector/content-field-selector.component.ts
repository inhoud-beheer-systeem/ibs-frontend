import { Observable } from 'rxjs';

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

// TODO: fix this, ew
import { ContentFieldQuery, ContentFieldService } from '../../../../app/core/store';

@Component({
    selector: 'app-content-field-selector',
    templateUrl: 'content-field-selector.component.html',
})
export class ContentFieldSelectorModalComponent implements OnInit {
    public contentFields$: any;
    public loading$: Observable<boolean>;

    constructor(
        private contentFieldService: ContentFieldService,
        private contentFieldQuery: ContentFieldQuery,
        public dialogRef: MatDialogRef<ContentFieldSelectorModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit(): void {
        this.contentFields$ = this.contentFieldQuery.results$;
        this.loading$ = this.contentFieldQuery.loading$;

        this.contentFieldService.fetchContentFields()
            .subscribe();
    }

    addField(field): void {
        this.dialogRef.close(field);
    }

    close(e: Event): void {
        e.preventDefault();
        this.dialogRef.close();
    }
}
