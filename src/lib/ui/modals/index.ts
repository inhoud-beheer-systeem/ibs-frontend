import { IconSelectModalComponent } from './icon-selector/icon-selector.component';
import { ResourceSelectorComponent } from './resource-selector/resource-selector.component';
import { ContentFieldSelectorModalComponent } from './content-field-selector/content-field-selector.component';

export { IconSelectModalComponent } from './icon-selector/icon-selector.component';
export { ResourceSelectorComponent } from './resource-selector/resource-selector.component';
export { ContentFieldSelectorModalComponent } from './content-field-selector/content-field-selector.component';

export const Modals = [
    IconSelectModalComponent,
    ResourceSelectorComponent,
    ContentFieldSelectorModalComponent
];
