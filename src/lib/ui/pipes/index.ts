import { PluralPipe } from './plural.pipe';
import { SingularPipe } from './singular.pipe';

export const Pipes = [
    PluralPipe,
    SingularPipe
];
