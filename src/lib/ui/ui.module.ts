import { MomentModule } from 'ngx-moment';
import { DragulaModule } from 'ng2-dragula';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgSelectModule } from '@ibsys/ng-select';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';

import { Components } from './components';
import { Forms } from './forms';
import { Modals } from './modals';
import { Pipes } from './pipes';
import { Services } from './services';
import { Directives } from './directives';

@NgModule({
    declarations: [
        Components,
        Pipes,
        Modals,
        Directives,
        Forms
    ],
    imports: [
        DragDropModule,
        CommonModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        MomentModule,
        NgSelectModule,
        CKEditorModule,
        LeafletModule.forRoot(),
        LeafletDrawModule.forRoot(),
        DragulaModule,
    ],
    exports: [
        Components,
        Pipes,
        Modals,
        Directives,
        Forms
    ],
    entryComponents: [
        Modals
    ],
    providers: [
        Services
    ],
})
export class UiModule { }
