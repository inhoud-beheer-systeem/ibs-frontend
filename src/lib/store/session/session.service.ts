import { SessionStore, Tenant, User } from './session.store';
import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {
    constructor(private sessionStore: SessionStore) {}

    updateTenant(tenant: Tenant) {
        this.sessionStore.update({ tenant });
    }

    updateUser(user: User) {
        this.sessionStore.update({ user });
    }

    updatePermissions(permissions) {
        this.sessionStore.update({ permissions });
    }
}
