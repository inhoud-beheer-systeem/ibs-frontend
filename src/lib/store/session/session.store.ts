import { Store, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';

export interface SessionState {
    tenant: Tenant;
    user: User;
    permissions: any[];
}

export interface Tenant {
    uuid: string;
    name: string;
    url: string;
}

export interface User {
    uuid: string;
    firstName: string;
    lastName: string;
}

export function createInitialState(): SessionState {
    return {
        tenant: null,
        user: null,
        permissions: [],
    };
}

@Injectable()
@StoreConfig({ name: 'session' })
export class SessionStore extends Store<SessionState> {
    constructor() {
        super(createInitialState());
    }
}
