import { Query } from '@datorama/akita';
import { SessionState, SessionStore } from './session.store';
import { Injectable } from '@angular/core';

@Injectable()
export class SessionQuery extends Query<SessionState> {
    public tenant$ = this.select('tenant');
    public user$ = this.select('user');
    public permissions$ = this.select('permissions');

    constructor(
        protected store: SessionStore
    ) {
        super(store);
    }
}
