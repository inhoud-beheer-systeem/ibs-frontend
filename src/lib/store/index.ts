import { ResourceQuery } from './resources/resources.query';
import { ResourceService } from './resources/resources.service';
import { ResourceStore } from './resources/resources.store';
import { ViewQuery } from './views/views.query';
import { ViewService } from './views/views.service';
import { ViewStore } from './views/views.store';
import { SessionQuery } from './session/session.query';
import { SessionService } from './session/session.service';
import { SessionStore } from './session/session.store';
import { StatusQuery } from './status/status.query';
import { StatusService } from './status/status.service';
import { StatusStore } from './status/status.store';
import { RoleQuery } from './roles/roles.query';
import { RoleService } from './roles/roles.service';
import { RoleStore } from './roles/roles.store';
import { PermissionQuery } from './permissions/permissions.query';
import { PermissionService } from './permissions/permissions.service';
import { PermissionStore } from './permissions/permissions.store';
import { LanguageQuery } from './languages/languages.query';
import { LanguageService } from './languages/languages.service';
import { LanguageStore } from './languages/languages.store';
import { WorkflowQuery } from './workflows/workflows.query';
import { WorkflowService } from './workflows/workflows.service';
import { WorkflowStore } from './workflows/workflows.store';
import { TaxonomyQuery } from './taxonomy/taxonomy.query';
import { TaxonomyService } from './taxonomy/taxonomy.service';
import { TaxonomyStore } from './taxonomy/taxonomy.store';

export { ResourceQuery } from './resources/resources.query';
export { ResourceService } from './resources/resources.service';
export { ResourceStore } from './resources/resources.store';
export { ViewQuery } from './views/views.query';
export { ViewService } from './views/views.service';
export { ViewStore } from './views/views.store';
export { SessionQuery } from './session/session.query';
export { SessionService } from './session/session.service';
export { SessionStore } from './session/session.store';
export { StatusQuery } from './status/status.query';
export { StatusService } from './status/status.service';
export { StatusStore } from './status/status.store';
export { RoleQuery } from './roles/roles.query';
export { RoleService } from './roles/roles.service';
export { RoleStore } from './roles/roles.store';
export { PermissionQuery } from './permissions/permissions.query';
export { PermissionService } from './permissions/permissions.service';
export { PermissionStore } from './permissions/permissions.store';
export { LanguageQuery } from './languages/languages.query';
export { LanguageService } from './languages/languages.service';
export { LanguageStore } from './languages/languages.store';
export { WorkflowQuery } from './workflows/workflows.query';
export { WorkflowService } from './workflows/workflows.service';
export { WorkflowStore } from './workflows/workflows.store';
export { TaxonomyQuery } from './taxonomy/taxonomy.query';
export { TaxonomyService } from './taxonomy/taxonomy.service';
export { TaxonomyStore } from './taxonomy/taxonomy.store';

export const StoreServices = [
    ResourceService,
    ViewService,
    SessionService,
    StatusService,
    RoleService,
    PermissionService,
    LanguageService,
    WorkflowService,
    TaxonomyService,
];

export const Stores = [
    ResourceStore,
    ViewStore,
    SessionStore,
    StatusStore,
    RoleStore,
    PermissionStore,
    LanguageStore,
    WorkflowStore,
    TaxonomyStore,
];

export const Queries = [
    ResourceQuery,
    ViewQuery,
    SessionQuery,
    StatusQuery,
    RoleQuery,
    PermissionQuery,
    LanguageQuery,
    WorkflowQuery,
    TaxonomyQuery,
];

