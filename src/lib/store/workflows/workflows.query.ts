import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';

import { Workflow, WorkflowState, WorkflowStore } from './workflows.store';

@Injectable()
export class WorkflowQuery extends QueryEntity<WorkflowState, Workflow> {
    constructor(protected store: WorkflowStore) {
        super(store);
    }
}
