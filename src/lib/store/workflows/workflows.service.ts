import { pathOr } from 'ramda';
import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { WorkflowStore } from './workflows.store';

@Injectable()
export class WorkflowService {

    constructor(
        private workflowStore: WorkflowStore,
        private http: HttpClient,
    ) { }

    create(values: any) {
        this.workflowStore.setLoading(true);
        return this.http.post<any>(`/api/v1/workflows`, values)
            .pipe(
                tap(result => {
                    this.workflowStore.add(result as any);
                    this.workflowStore.setLoading(false);
                })
            );
    }

    fetch() {
        this.workflowStore.setLoading(true);
        return this.http.get<any>(`/api/v1/workflows`)
            .pipe(
                tap(result => {
                    this.workflowStore.set(result._embedded);
                    this.workflowStore.setLoading(false);
                })
            );
    }

    fetchOne(userId: string) {
        this.workflowStore.setLoading(true);
        return this.http.get<any>(`/api/v1/workflows/${userId}`)
            .pipe(
                tap(result => {
                    this.workflowStore.set([result as any]);
                    this.workflowStore.setLoading(false);
                })
            );
    }

    update(userId: string, values: any) {
        this.workflowStore.setLoading(true);
        return this.http.put<any>(
            `/api/v1/workflows/${userId}`,
            values
        )
            .pipe(
                tap(() => {
                    this.workflowStore.set([values]);
                    this.workflowStore.setLoading(false);
                })
            );
    }

    delete(userId: string) {
        this.workflowStore.setLoading(true);
        return this.http.delete<any>(`/api/v1/workflows/${userId}`)
            .pipe(
                tap(result => {
                    this.workflowStore.remove(userId);
                    this.workflowStore.setLoading(false);
                })
            );
    }
}






