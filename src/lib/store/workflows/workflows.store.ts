import { EntityState, EntityStore, ID, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';

export interface Workflow {
    uuid: string;
    name: string;
    slug: string;
    description: string;
    states: any[];
    createdAt: Date;
    updatedAt: Date;
}

export interface WorkflowState extends EntityState<Workflow> { }

@Injectable()
@StoreConfig({ name: 'workflows', idKey: 'uuid' })
export class WorkflowStore extends EntityStore<WorkflowState, Workflow> {
  constructor() {
    super();
  }
}
