import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';

import { Taxonomy, TaxonomyState, TaxonomyStore } from './taxonomy.store';

@Injectable()
export class TaxonomyQuery extends QueryEntity<TaxonomyState, Taxonomy> {
    constructor(protected store: TaxonomyStore) {
        super(store);
    }
}
