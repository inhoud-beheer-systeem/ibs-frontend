import { EntityState, EntityStore, ID, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';

export interface Taxonomy {
    uuid: string;
    name: string;
    slug: string;
    description: string;
    items: TaxonomyItem[];
    createdAt: Date;
    updatedAt: Date;
}

export interface TaxonomyItem {
    uuid: string;
    value: string;
    label: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface TaxonomyState extends EntityState<Taxonomy> { }

@Injectable()
@StoreConfig({ name: 'taxonomy', idKey: 'uuid' })
export class TaxonomyStore extends EntityStore<TaxonomyState, Taxonomy> {
  constructor() {
    super();
  }
}
