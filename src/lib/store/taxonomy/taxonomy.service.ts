import { pathOr } from 'ramda';
import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { TaxonomyStore } from './taxonomy.store';

@Injectable()
export class TaxonomyService {

    constructor(
        private taxonomyStore: TaxonomyStore,
        private http: HttpClient,
    ) { }

    create(values: any) {
        this.taxonomyStore.setLoading(true);
        return this.http.post<any>(`/api/v1/taxonomy`, values)
            .pipe(
                tap(result => {
                    this.taxonomyStore.add(result as any);
                    this.taxonomyStore.setLoading(false);
                })
            );
    }

    fetch() {
        this.taxonomyStore.setLoading(true);
        return this.http.get<any>(`/api/v1/taxonomy`)
            .pipe(
                tap(result => {
                    this.taxonomyStore.set(result._embedded);
                    this.taxonomyStore.setLoading(false);
                })
            );
    }

    fetchOne(userId: string) {
        this.taxonomyStore.setLoading(true);
        return this.http.get<any>(`/api/v1/taxonomy/${userId}`)
            .pipe(
                tap(result => {
                    this.taxonomyStore.set([result as any]);
                    this.taxonomyStore.setLoading(false);
                })
            );
    }

    update(userId: string, values: any) {
        this.taxonomyStore.setLoading(true);
        return this.http.put<any>(
            `/api/v1/taxonomy/${userId}`,
            values
        )
            .pipe(
                tap(() => {
                    this.taxonomyStore.set([values]);
                    this.taxonomyStore.setLoading(false);
                })
            );
    }

    delete(userId: string) {
        this.taxonomyStore.setLoading(true);
        return this.http.delete<any>(`/api/taxonomy/${userId}`)
            .pipe(
                tap(result => {
                    this.taxonomyStore.remove(userId);
                    this.taxonomyStore.setLoading(false);
                })
            );
    }
}






