import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ViewStore } from './views.store';

@Injectable()
export class ViewService {

    constructor(
        private viewStore: ViewStore,
        private http: HttpClient
    ) { }

    create(values) {
        this.viewStore.setLoading(true);
        return this.http.post<any>('/api/v1/views', values)
            .pipe(
                tap(result => {
                    this.viewStore.add(result);
                    this.viewStore.setLoading(false);
                })
            );
    }

    fetch() {
        this.viewStore.setLoading(true);
        return this.http.get<any>('/api/v1/views' )
            .pipe(
                tap(result => {
                    this.viewStore.set(result._embedded);
                    this.viewStore.setLoading(false);
                })
            );
    }

    fetchOne(id) {
        this.viewStore.setLoading(true);
        return this.http.get<any>(`/api/v1/views/${id}`)
            .pipe(
                tap(result => {
                    this.viewStore.update([result]);
                    this.viewStore.setLoading(false);
                })
            );
    }

    update(id: string, values: any) {
        this.viewStore.setLoading(true);
        return this.http.put<any>(`/api/v1/views/${id}`, values)
            .pipe(
                tap((result) => {
                    this.viewStore.update(result);
                    this.viewStore.setLoading(false);
                })
            );
    }

    delete(id: string) {
        this.viewStore.setLoading(true);
        return this.http.delete<any>(`/api/v1/views/${id}`)
            .pipe(
                tap(() => {
                    this.viewStore.remove(id);
                    this.viewStore.setLoading(false);
                })
            );
    }
}
