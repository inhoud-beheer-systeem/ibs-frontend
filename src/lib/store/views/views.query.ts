import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';

import { View, ViewState, ViewStore } from './views.store';

@Injectable()
export class ViewQuery extends QueryEntity<ViewState, View> {
    constructor(protected store: ViewStore) {
        super(store);
    }
}
