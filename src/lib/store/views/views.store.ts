import { EntityState, EntityStore, ID, StoreConfig } from '@datorama/akita';
import { Injectable } from "@angular/core";

export interface View {
    name: string;
    type: string;
    mode: number;
    accessTime: string;
    modifyTime: string;
    changeTime: string;
    isFolder: boolean;
    isImag: boolean;
    extension?: string;
}

export interface ViewState extends EntityState<View> { }

@Injectable()
@StoreConfig({ name: 'views', idKey: 'uuid' })
export class ViewStore extends EntityStore<ViewState, View> {
  constructor() {
    super();
  }
}
