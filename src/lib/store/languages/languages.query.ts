import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';

import { Language, LanguageState, LanguageStore } from './languages.store';

@Injectable()
export class LanguageQuery extends QueryEntity<LanguageState, Language> {
    constructor(protected store: LanguageStore) {
        super(store);
    }
}
