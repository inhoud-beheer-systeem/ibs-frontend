import { tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { LanguageStore } from './languages.store';

@Injectable()
export class LanguageService {

    constructor(
        private languageStore: LanguageStore,
        private http: HttpClient
    ) { }

    fetchActive() {
        this.languageStore.setLoading(true);
        return this.http.get<any>('/api/v1/languages/active' )
            .pipe(
                tap(result => {
                    this.languageStore.set(result._embedded);
                    this.languageStore.setLoading(false);
                })
            );
    }
}
