import { EntityState, EntityStore, ID, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';

export interface Language {
    key: string;
    label: string;
}

export interface LanguageState extends EntityState<Language> { }

@Injectable()
@StoreConfig({ name: 'languages', idKey: 'key' })
export class LanguageStore extends EntityStore<LanguageState, Language> {
  constructor() {
    super();
  }
}
